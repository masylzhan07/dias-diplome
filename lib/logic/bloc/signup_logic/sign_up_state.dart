import 'package:dias_diplome/data/models/role_model.dart';
import 'package:dias_diplome/presentation/widgets/form_submission_status.dart';

class SignUpState {
  final String name;
  bool get isValidUserName => name.length > 3;

  final String surname;
  bool get isValidUserSurname => surname.length > 3;

  final String phone;
  bool get isValidUserPhone => phone.length > 3;

  final String email;
  bool get isValidEmail => email.contains('@');

  final int roleId;
  // bool get isValidRole => email.contains('@');

  final String password;
  bool get isValidPassword => password.length > 6;

  final FormSubmissionStatus formStatus;

  SignUpState({
    this.name = '',
    this.surname = '',
    this.email = '',
    this.phone = '',
    this.password = '',
    this.formStatus = const InitialFormStatus(),
    this.roleId = 103,
  });

  SignUpState copyWith({
    String? name,
    String? surname,
    String? phone,
    String? email,
    String? password,
    FormSubmissionStatus? formStatus,
    int? roleId,
  }) {
    return SignUpState(
      name: name ?? this.name,
      surname: surname ?? this.surname,
      phone: phone ?? this.phone,
      email: email ?? this.email,
      password: password ?? this.password,
      formStatus: formStatus ?? this.formStatus,
      roleId: roleId ?? this.roleId,
    );
  }
}
