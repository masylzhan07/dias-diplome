import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/providers/user_provider.dart';
import 'package:dias_diplome/logic/cubit/auth_logic/auth_cubit.dart';
import 'package:dias_diplome/logic/cubit/navigation_logic/navigation_cubit.dart';
import 'package:dias_diplome/logic/cubit/session_logic/session_cubit.dart';
import 'package:dias_diplome/logic/cubit/session_logic/session_state.dart';
import 'package:dias_diplome/logic/device_access.dart';
import 'package:dias_diplome/presentation/screens/user_loader_screen.dart';
import 'package:dias_diplome/presentation/widgets/bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import 'auth_navigator.dart';
import 'loading_view.dart';

class AppNavigator extends StatefulWidget {
  @override
  State<AppNavigator> createState() => _AppNavigatorState();
}

class _AppNavigatorState extends State<AppNavigator> {
  bool _isHandleRequest = true;
  String deviceId = '';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadData();
  }

  void _loadData() async {
    var responseData = await getDeviceId();
    Future.delayed(Duration(seconds: 5)).then((value) => setState(() {
          deviceId = responseData ?? '';
          _isHandleRequest = false;
        }));
  }

  @override
  Widget build(BuildContext context) {
    if (_isHandleRequest) {
      return Scaffold(
        backgroundColor: PRIMARY_COLOR,
        body: Center(child: Lottie.asset('assets/images/shopping-gif.json')),
      );
    }
    UserProvider _userProvider = Provider.of<UserProvider>(context);

    _userProvider.setDeviceId(deviceId);

    return BlocBuilder<SessionCubit, SessionState>(builder: (context, state) {
      return Navigator(
        pages: [
          // Show loading screen
          if (state is UnknownSessionState)
            MaterialPage(
                child: Scaffold(
              backgroundColor: PRIMARY_COLOR,
              body: Center(
                  child: Lottie.asset('assets/images/shopping-gif.json')),
            )),

          // Show auth flow
          if (state is Unauthenticated)
            MaterialPage(
              child: BlocProvider(
                create: (context) =>
                    AuthCubit(sessionCubit: context.read<SessionCubit>()),
                child: const AuthNavigator(),
              ),
            ),

          if (state is Guest)
            MaterialPage(
              child: const CustomBottomNavigation(),
            ),

          // Show session flow
          if (state is Authenticated)
            MaterialPage(child: const UserLoaderScreen()

                // ,
                // SessionView(
                //   userToken: state.userToken,
                // ),
                )
        ],
        onPopPage: (route, result) => route.didPop(result),
      );
    });
  }
}
