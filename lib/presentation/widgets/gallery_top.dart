import 'package:cached_network_image/cached_network_image.dart';
import 'package:dias_diplome/data/models/image_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GalleryTop extends StatefulWidget {
  final List<ImageModel>? gallery;
  const GalleryTop({
    Key? key,
    required this.gallery,
  }) : super(key: key);

  @override
  _GalleryTopState createState() => _GalleryTopState();
}

class _GalleryTopState extends State<GalleryTop> {
  int _current = 0;
  bool? _isFav;

  Widget indicator() => Container(
        constraints:
            BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List<Widget>.generate(
              widget.gallery!.length,
              (index) => Expanded(
                    flex: _current.round() == index ? 2 : 1,
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 25),
                      child: Stack(
                        children: [
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onTap: () {
                              setState(() {
                                _current = index;
                              });
                            },
                            child: Container(
                              height: 40,
                              width: 40,
                            ),
                          ),
                          Positioned.fill(
                            child: Align(
                              alignment: Alignment.center,
                              child: InkWell(
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () {
                                  setState(() {
                                    _current = index;
                                  });
                                },
                                child: _current.round() == index
                                    ? Container(
                                        height: 8.0,
                                        // width: 77.0,
                                        // padding: EdgeInsets.all(2),
                                        decoration: BoxDecoration(
                                          color:
                                              Color.fromRGBO(255, 255, 255, 1),
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                      )
                                    : Container(
                                        height: 8.0,
                                        // width: 31.0,
                                        decoration: BoxDecoration(
                                          color: Color.fromRGBO(
                                              255, 255, 255, 0.47),
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                      ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )),
        ),
      );

  void _next() {
    setState(() {
      if (_current < widget.gallery!.length - 1) {
        _current++;
      } else {
        _current = _current;
      }
    });
  }

  void _prev() {
    setState(() {
      if (_current > 0) {
        _current--;
      } else {
        _current = 0;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      child: AnimatedSwitcher(
        duration: Duration(milliseconds: 500),
        transitionBuilder: (Widget child, Animation<double> animation) {
          return FadeTransition(child: child, opacity: animation);
        },
        child: GestureDetector(
          key: ValueKey<int>(_current),
          onHorizontalDragEnd: (DragEndDetails details) {
            if (details.velocity.pixelsPerSecond.dx > 0) {
              _prev();
            } else if (details.velocity.pixelsPerSecond.dx < 0) {
              _next();
            }
          },
          child: Stack(
            children: [
              widget.gallery!.isEmpty
                  ? SizedBox()
                  : CachedNetworkImage(
                      imageUrl: widget.gallery![_current].src ?? '',
                      // cacheManager: cacheDuration,
                      fit: BoxFit.cover,
                      height: MediaQuery.of(context).size.height * 0.5,
                      width: MediaQuery.of(context).size.width,
                      placeholder: (context, url) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      },
                      errorWidget: (context, url, error) {
                        print(error);
                        return Center(
                          child: Text('Error'),
                        );
                      },
                      imageBuilder: (context, imageProvider) {
                        return ClipRRect(
                          child: Image(
                            image: imageProvider,
                            fit: BoxFit.cover,
                            height: MediaQuery.of(context).size.height * 0.5,
                            width: MediaQuery.of(context).size.width,
                          ),
                        );
                      },
                    ),
              Positioned.fill(
                child: Container(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color.fromRGBO(0, 0, 0, 0.35),
                        Color.fromRGBO(0, 0, 0, 0),
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                ),
              ),
              if (widget.gallery!.length > 1)
                Container(
                  margin: EdgeInsets.all(30),
                  alignment: Alignment.bottomCenter,
                  child: indicator(),
                )
            ],
          ),
        ),
      ),
    );
  }
}
