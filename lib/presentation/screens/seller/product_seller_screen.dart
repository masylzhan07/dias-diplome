import 'package:dias_diplome/data/models/product_model.dart';
import 'package:dias_diplome/data/providers/api.dart';
import 'package:dias_diplome/data/providers/user_provider.dart';
import 'package:dias_diplome/data/repositories/basket_repository.dart';
import 'package:dias_diplome/presentation/widgets/gallery_top.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';

import 'product_create_screen.dart';

class ProductSellerScreen extends StatelessWidget {
  final Product product;
  const ProductSellerScreen({Key? key, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    UserProvider _userProvider = Provider.of<UserProvider>(context);
    return Scaffold(
      extendBody: true,
      body: Stack(
        fit: StackFit.expand,
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GalleryTop(gallery: product.images),
                Padding(
                  padding: const EdgeInsets.all(30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${product.name}'),
                      SizedBox(height: 5),
                      Text('${product.category!.name}'),
                      SizedBox(height: 5),
                      Text('${product.price}'),
                      SizedBox(height: 5),
                      if (product.colors!.isNotEmpty)
                        Padding(
                          padding: const EdgeInsets.only(bottom: 5),
                          child: Text('${product.colors?.first.name}'),
                        ),
                      Text('${product.size!.name}'),
                    ],
                  ),
                ),
              ],
            ),
          ),
          if (_userProvider.roleId != 102)
            Positioned(
              left: 30,
              right: 30,
              bottom: 130,
              child: ElevatedButton(
                onPressed: () async {
                  const FlutterSecureStorage _storage = FlutterSecureStorage();
                  String? token = await _storage.read(key: 'accessToken');
                  token = _userProvider.deviceId;
                  final response =
                      await APIRepository().addToBasket(FormData.fromMap({
                    'product_id': product.id,
                    'filter[]': [],
                    'filter[app_number]': token!,
                  }));
                  if (response.statusCode == 200) {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        behavior: SnackBarBehavior.floating,
                        backgroundColor: Colors.green,
                        content: Text('Товар добавлен в корзину')));
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        behavior: SnackBarBehavior.floating,
                        backgroundColor: Colors.red,
                        content: Text('Ошибка! Попробуйте позже')));
                  }
                },
                child: Text('Добавить в корзину'),
              ),
            )
        ],
      ),
    );
  }
}
