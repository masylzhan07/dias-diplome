import 'dart:convert';
import 'dart:io';

import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/models/category_model.dart';
import 'package:dias_diplome/data/models/material_model.dart';
import 'package:dias_diplome/data/providers/api.dart';
import 'package:dias_diplome/data/repositories/categories_repository.dart';
import 'package:dias_diplome/data/repositories/colors_repository.dart';
import 'package:dias_diplome/data/repositories/materials_repository.dart';
import 'package:dias_diplome/data/repositories/sizes_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';

class ProductCreateScreen extends StatefulWidget {
  const ProductCreateScreen({Key? key}) : super(key: key);

  @override
  _ProductCreateScreenState createState() => _ProductCreateScreenState();
}

class _ProductCreateScreenState extends State<ProductCreateScreen> {
  TextEditingController _productNameController = TextEditingController();
  TextEditingController _productBrandController = TextEditingController();
  TextEditingController _productSizeController = TextEditingController();
  TextEditingController _productPriceController = TextEditingController();
  TextEditingController _productCountController = TextEditingController();
  List<Category> _categories = [];
  Category? _currentCategory;
  List<Category> _subCategories = [];
  Category? _currentSubCategory;
  List<MaterialModel> _materials = [];
  int _currentMaterialId = 0;
  List<MaterialModel> _colors = [];
  int _currentColorId = 0;
  List<MaterialModel> _sizes = [];
  int _currentSizeId = 0;
  List<File> _images = [];
  List<MultipartFile> _uploadImages = [];
  late ImagePicker picker;

  bool _isHandleRequest = true;
  bool _isHandleRequestError = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    picker = ImagePicker();

    _loadData();
  }

  void _loadData() async {
    try {
      final responseCategories = await CategoriesRepository().fetchCategories();
      final responseMaterials = await MaterialsRepository().fetchMaterials();
      final responseColors = await ColorsRepository().fetchColors();
      final responseSizes = await SizesRepository().fetchSizes();
      setState(() {
        _categories.addAll(responseCategories);
        // _currentCategory = _categories.first;
        _materials.addAll(responseMaterials);
        _currentMaterialId = _materials.first.id ?? 0;

        _colors.addAll(responseColors);
        _currentColorId = _colors.first.id ?? 0;

        _sizes.addAll(responseSizes);
        _currentSizeId = _sizes.first.id ?? 0;

        _isHandleRequest = false;
      });
    } on Exception catch (e) {
      setState(() {
        _isHandleRequest = false;
        _isHandleRequestError = true;
      });
      throw Exception(e);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _productNameController.dispose();
    _productBrandController.dispose();
    _productSizeController.dispose();
    _productPriceController.dispose();
    _productCountController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_isHandleRequest) {
      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    if (_isHandleRequestError) {
      return Scaffold(
        body: Center(
          child: Text('Ошибка'),
        ),
      );
    } else {
      return Scaffold(
        extendBody: true,
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _productNameField(),
                SizedBox(height: 20),
                _productBrandField(),
                SizedBox(height: 20),
                _productSizeField(),
                SizedBox(height: 20),
                _productCategoryField(),
                SizedBox(height: 20),
                if (_subCategories.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: _productSubCategoryField(),
                  ),
                _productMaterialField(),
                SizedBox(height: 20),
                _productColorField(),
                SizedBox(height: 20),
                _productCountField(),
                SizedBox(height: 20),
                _productPriceField(),
                SizedBox(height: 20),
                _productImagesField(),
                SizedBox(height: 20),
                Center(
                  child: ElevatedButton(
                    onPressed: () async {
                      final response =
                          await APIRepository().createProduct(FormData.fromMap({
                        'category_id': _currentSubCategory != null
                            ? _currentSubCategory!.id
                            : _currentCategory!.id,
                        'name': _productNameController.text,
                        'brand': _productBrandController.text,
                        'count': _productCountController.text,
                        'price': _productPriceController.text,
                        'materials[]': [_currentMaterialId],
                        'state_id': 1,
                        'size_id': _currentSizeId,
                        'colors[]': [_currentColorId],
                        'images[]': _uploadImages,
                      }));
                      if (response.statusCode == 200) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            backgroundColor: Colors.green,
                            content: Text('Товар успешно  создан')));
                        Navigator.popUntil(context, (route) => route.isFirst);
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            backgroundColor: Colors.red,
                            content: Text('Ошибка! Попробуйте позже')));
                      }
                    },
                    child: Text('Создать товар'),
                  ),
                ),
                SizedBox(height: 130),
              ],
            ),
          ),
        ),
      );
    }
  }

  Widget _productNameField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Введите название товара'),
        SizedBox(height: 5),
        TextFormField(
          decoration: InputDecoration(
            hintText: 'Название',
            isDense: true,
            contentPadding: EdgeInsets.all(10),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          controller: _productNameController,
        ),
      ],
    );
  }

  Widget _productBrandField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Введите бренд товара'),
        SizedBox(height: 5),
        TextFormField(
          decoration: InputDecoration(
            hintText: 'Бренд',
            isDense: true,
            contentPadding: EdgeInsets.all(10),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          controller: _productBrandController,
        ),
      ],
    );
  }

  Widget _productCountField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Введите количество товара'),
        SizedBox(height: 5),
        TextFormField(
          decoration: InputDecoration(
            hintText: 'Количество',
            isDense: true,
            contentPadding: EdgeInsets.all(10),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          controller: _productCountController,
        ),
      ],
    );
  }

  Widget _productSizeField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Выберите размер товара'),
        SizedBox(height: 5),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
          child: DropdownButton<int>(
            value: _currentSizeId,
            icon: Container(
              margin: const EdgeInsets.only(left: 10),
              child: const Icon(Icons.keyboard_arrow_down),
            ),
            isExpanded: true,
            menuMaxHeight: 220,
            iconEnabledColor: const Color.fromRGBO(63, 136, 239, 1),
            dropdownColor: PRIMARY_COLOR,
            underline: const SizedBox(),
            onChanged: (int? value) {
              setState(() {
                _currentSizeId = value ?? 0;
              });
            },
            items: _sizes
                .map((value) => DropdownMenuItem<int>(
                      value: value.id,
                      child: Text(
                        '${value.name}',
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
      ],
    );
  }

  Widget _productCategoryField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Выберите категорию'),
        SizedBox(height: 5),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
          child: DropdownButton<Category>(
            value: _currentCategory,
            icon: Container(
              margin: const EdgeInsets.only(left: 10),
              child: const Icon(Icons.keyboard_arrow_down),
            ),
            isExpanded: true,
            menuMaxHeight: 220,
            iconEnabledColor: const Color.fromRGBO(63, 136, 239, 1),
            dropdownColor: PRIMARY_COLOR,
            underline: const SizedBox(),
            onChanged: (Category? value) {
              setState(() {
                _currentCategory = value;
                if (value!.childs != null) {
                  _subCategories = value.childs!;
                } else {
                  _subCategories = [];
                  _currentSubCategory = null;
                }
              });
            },
            items: _categories
                .map((value) => DropdownMenuItem<Category>(
                      value: value,
                      child: Text(
                        '${value.name}',
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
      ],
    );
  }

  Widget _productSubCategoryField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Выберите категорию'),
        SizedBox(height: 5),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
          child: DropdownButton<Category>(
            value: _currentSubCategory,
            icon: Container(
              margin: const EdgeInsets.only(left: 10),
              child: const Icon(Icons.keyboard_arrow_down),
            ),
            isExpanded: true,
            menuMaxHeight: 220,
            iconEnabledColor: const Color.fromRGBO(63, 136, 239, 1),
            dropdownColor: PRIMARY_COLOR,
            underline: const SizedBox(),
            onChanged: (Category? value) {
              setState(() {
                _currentSubCategory = value;
              });
            },
            items: _subCategories
                .map((value) => DropdownMenuItem<Category>(
                      value: value,
                      child: Text(
                        '${value.name}',
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
      ],
    );
  }

  Widget _productMaterialField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Выберите материал'),
        SizedBox(height: 5),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
          child: DropdownButton<int>(
            value: _currentMaterialId,
            icon: Container(
              margin: const EdgeInsets.only(left: 10),
              child: const Icon(Icons.keyboard_arrow_down),
            ),
            isExpanded: true,
            menuMaxHeight: 220,
            iconEnabledColor: const Color.fromRGBO(63, 136, 239, 1),
            dropdownColor: PRIMARY_COLOR,
            underline: const SizedBox(),
            onChanged: (int? value) {
              setState(() {
                _currentMaterialId = value ?? 0;
              });
            },
            items: _materials
                .map((value) => DropdownMenuItem<int>(
                      value: value.id,
                      child: Text(
                        '${value.name}',
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
      ],
    );
  }

  Widget _productColorField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Выберите цвет'),
        SizedBox(height: 5),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
          child: DropdownButton<int>(
            value: _currentColorId,
            icon: Container(
              margin: const EdgeInsets.only(left: 10),
              child: const Icon(Icons.keyboard_arrow_down),
            ),
            isExpanded: true,
            menuMaxHeight: 220,
            iconEnabledColor: const Color.fromRGBO(63, 136, 239, 1),
            dropdownColor: PRIMARY_COLOR,
            underline: const SizedBox(),
            onChanged: (int? value) {
              setState(() {
                _currentColorId = value ?? 0;
              });
            },
            items: _colors
                .map((value) => DropdownMenuItem<int>(
                      value: value.id,
                      child: Text(
                        '${value.name}',
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
      ],
    );
  }

  Widget _productPriceField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Введите цену товара'),
        SizedBox(height: 5),
        TextFormField(
          decoration: InputDecoration(
            hintText: 'Цена',
            isDense: true,
            contentPadding: EdgeInsets.all(10),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          controller: _productPriceController,
        ),
      ],
    );
  }

  Widget _productImagesField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Выберите фото товара'),
        SizedBox(height: 5),
        Wrap(
          spacing: 10,
          runSpacing: 10,
          children: [
            ..._images.map(
              (img) => Image.file(
                img,
                width: 100.0,
                height: 100.0,
                fit: BoxFit.cover,
              ),
            ),
            GestureDetector(
              onTap: () async {
                XFile? image =
                    await picker.pickImage(source: ImageSource.gallery);
                // String uploadImage = base64Encode(await image!.readAsBytes());
                // _uploadImages.add(await MultipartFile.fromFile(image!.path));

                MultipartFile mFile = await MultipartFile.fromFile(
                  image!.path,
                  filename: image.path.split('/').last,
                );

                setState(() {
                  // _images = File(image.path);
                  // for (var element in images!) {
                  _images.add(File(image.path));
                  _uploadImages.add(mFile);
                  // }
                });
              },
              child: Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: PRIMARY_COLOR.withOpacity(0.5),
                ),
                alignment: Alignment.center,
                child: Icon(Icons.add_a_photo),
              ),
            )
          ],
        ),
      ],
    );
  }
}
