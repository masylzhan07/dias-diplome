import 'dart:io';

import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/models/seller_model.dart';
import 'package:dias_diplome/data/models/user_model.dart';
import 'package:dias_diplome/data/providers/api.dart';
import 'package:dias_diplome/data/providers/user_provider.dart';
import 'package:dias_diplome/data/repositories/profile_repository.dart';
import 'package:dias_diplome/logic/cubit/session_logic/session_cubit.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class ProfileSellerScreen extends StatefulWidget {
  const ProfileSellerScreen({Key? key}) : super(key: key);

  @override
  _ProfileSellerScreenState createState() => _ProfileSellerScreenState();
}

class _ProfileSellerScreenState extends State<ProfileSellerScreen> {
  late UserProvider _userProvider;
  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    return Scaffold(
      extendBody: true,
      body: FutureBuilder(
        future: ProfileRepository().fetchProfile(),
        builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
          if (snapshot.hasData) {
            _userProvider.setUserId(snapshot.data!.id!);
            return SingleChildScrollView(
              padding: EdgeInsets.only(bottom: 130),
              child: Column(
                children: [
                  snapshot.data!.background_image_of_shop!.isEmpty
                      ? Container(
                          height: MediaQuery.of(context).size.height * 0.5,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey,
                          alignment: Alignment.center,
                          child: Text('Нет фото'),
                        )
                      : Image.network(
                          '${snapshot.data!.background_image_of_shop}',
                          height: MediaQuery.of(context).size.height * 0.5,
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.cover,
                        ),
                  SizedBox(height: 30),
                  ...[
                    {
                      'tag': 'Название: ',
                      'name': snapshot.data!.name_of_shop,
                    },
                    {
                      'tag': 'Номер: ',
                      'name': snapshot.data!.phone,
                    },
                    {
                      'tag': 'Адрес: ',
                      'name': snapshot.data!.address_of_shop,
                    },
                  ]
                      .map(
                        (val) => Container(
                          margin: EdgeInsets.only(bottom: 20),
                          child: Row(
                            children: [
                              Text(
                                '${val['tag']}',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Text(
                                '${val['name']}',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                      .toList(),
                  Center(
                    child: ElevatedButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            builder: (_) {
                              return UpdateSellerProfileDialog(
                                  user: snapshot.data!);
                            }).then((value) {
                          setState(() {});
                        });
                      },
                      child: Text('Редактировать профиль'),
                    ),
                  ),
                  Center(
                    child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.red)),
                      onPressed: () {
                        BlocProvider.of<SessionCubit>(context).signOut();
                      },
                      child: Text('Выйти из профиля'),
                    ),
                  )
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Ошибка'),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class UpdateSellerProfileDialog extends StatefulWidget {
  final User user;
  const UpdateSellerProfileDialog({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  State<UpdateSellerProfileDialog> createState() =>
      _UpdateSellerProfileDialogState();
}

class _UpdateSellerProfileDialogState extends State<UpdateSellerProfileDialog> {
  late TextEditingController _nameController;
  TextEditingController _addressController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  File? _images;
  MultipartFile? _uploadImages;

  ImagePicker picker = ImagePicker();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController(text: widget.user.name_of_shop);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      title: const Text('Редактирование профиля'),
      content: Form(
        key: GlobalKey<FormState>(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextFormField(
              controller: _nameController,
              decoration: const InputDecoration(
                hintText: 'Название',
                isDense: true,
                contentPadding: EdgeInsets.all(20),
                filled: true,
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
              ),
            ),
            const SizedBox(height: 15),
            TextFormField(
              controller: _phoneController,
              decoration: const InputDecoration(
                hintText: 'Номер',
                isDense: true,
                contentPadding: EdgeInsets.all(20),
                filled: true,
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
              ),
            ),
            const SizedBox(height: 15),
            TextFormField(
              controller: _addressController,
              decoration: const InputDecoration(
                hintText: 'Адрес',
                isDense: true,
                contentPadding: EdgeInsets.all(20),
                filled: true,
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
              ),
            ),
            const SizedBox(height: 15),
            Column(
              children: [
                TextButton(
                  onPressed: () async {
                    XFile? image =
                        await picker.pickImage(source: ImageSource.gallery);
                    // String uploadImage = base64Encode(await image!.readAsBytes());
                    // _uploadImages.add(await MultipartFile.fromFile(image!.path));

                    MultipartFile mFile = await MultipartFile.fromFile(
                      image!.path,
                      filename: image.path.split('/').last,
                    );

                    setState(() {
                      // _images = File(image.path);
                      // for (var element in images!) {
                      _images = File(image.path);
                      _uploadImages = mFile;
                      // }
                    });
                  },
                  child: Text('Изменить фото'),
                ),
                _images != null
                    ? Image.file(
                        _images!,
                        width: 100.0,
                        height: 100.0,
                        fit: BoxFit.cover,
                      )
                    : Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: PRIMARY_COLOR.withOpacity(0.5),
                        ),
                      ),
              ],
            ),
            const SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Отменить',
                  ),
                ),
                SizedBox(width: 10),
                ElevatedButton(
                  onPressed: () async {
                    await APIRepository()
                        .updateSellerProfile(
                      widget.user.id,
                      // {
                      // 'name': _nameController.text,
                      // 'address_of_shop': _addressController.text,
                      // 'phone': _phoneController.text,
                      // },
                      FormData.fromMap({
                        '_method': 'put',
                        'background_image_of_shop': _uploadImages,
                        'name_of_shop': _nameController.text,
                        'address_of_shop': _addressController.text,
                        // 'phone': _phoneController.text,
                      }),
                    )
                        .then((value) {
                      Navigator.of(context).pop(value.statusCode == 200);
                    });
                  },
                  child: Text(
                    'Обновить',
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
