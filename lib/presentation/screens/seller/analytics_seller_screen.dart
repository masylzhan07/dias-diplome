import 'package:dias_diplome/data/models/analytics_model.dart';
import 'package:dias_diplome/data/repositories/analytics_repository.dart';
import 'package:flutter/material.dart';

class AnalyticsSellerScreen extends StatelessWidget {
  const AnalyticsSellerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: FutureBuilder(
          future: AnalyticsRepository().fetchAnalytics(),
          builder: (context, AsyncSnapshot<AnalyticsModel> snapshot) {
            if (snapshot.hasData) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 30, vertical: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text(
                              'Общая сумма товаров',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                            Text(
                              '${snapshot.data!.total}',
                              style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text(
                              'Прибыль',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                            Text(
                              '${snapshot.data!.profit}',
                              style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Text(
                      'Товары',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      itemCount: snapshot.data!.products?.length ?? 0,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: EdgeInsets.only(top: 15),
                          height: 200,
                          width: double.infinity,
                          child: Row(
                            children: [
                              Flexible(
                                flex: 2,
                                child: Image.network(
                                  '${snapshot.data!.products![index].images!.first.src}',
                                  height: double.infinity,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              const SizedBox(width: 10),
                              Flexible(
                                flex: 3,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Название: ${snapshot.data!.products![index].name}',
                                      maxLines: 2,
                                    ),
                                    Text(
                                      'Категория: ${snapshot.data!.products![index].category!.name}',
                                      maxLines: 2,
                                    ),
                                    Text(
                                      'Размер: ${snapshot.data!.products![index].size!.name}',
                                      maxLines: 2,
                                    ),
                                    Text(
                                      'Материалы: ${snapshot.data!.products![index].materials!.fold('', (previousValue, element) => previousValue == '' ? '${element.name}' : '$previousValue, ${element.name}')}',
                                      maxLines: 2,
                                    ),
                                    Text(
                                      'Цвета: ${snapshot.data!.products![index].colors!.fold('', (previousValue, element) => previousValue == '' ? '${element.name}' : '$previousValue, ${element.name}')}',
                                      maxLines: 2,
                                    ),
                                    Text(
                                      'Количество: ${snapshot.data!.products![index].count}',
                                      maxLines: 2,
                                    ),
                                    Text(
                                      'Цена: ${snapshot.data!.products![index].price}',
                                      maxLines: 2,
                                    ),
                                    const SizedBox(height: 10),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          'Продано: ${snapshot.data!.products![index].sold} шт.',
                                          maxLines: 2,
                                          style: const TextStyle(
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          'Прибыль: ${snapshot.data!.products![index].total}',
                                          maxLines: 2,
                                          style: const TextStyle(
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  )
                ],
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Text('Ошибка'),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
