import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/providers/user_provider.dart';
import 'package:dias_diplome/logic/device_access.dart';
import 'package:dias_diplome/presentation/screens/seller/analytics_seller_screen.dart';
import 'package:dias_diplome/presentation/screens/seller/products_seller_screen.dart';
import 'package:dias_diplome/presentation/screens/seller/profile_seller_screen.dart';
import 'package:dias_diplome/presentation/widgets/bottom_navshape.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

enum Tabs {
  Profile,
  Analytics,
  Products,
}

class BottomNavbarSeller extends StatefulWidget {
  static const routeName = '/bottom-nav-seller';

  const BottomNavbarSeller({Key? key}) : super(key: key);

  @override
  _BottomNavbarSellerState createState() => _BottomNavbarSellerState();
}

class _BottomNavbarSellerState extends State<BottomNavbarSeller> {
  var _currentTab = Tabs.Profile;
  final _navigatorKeys = {
    Tabs.Profile: GlobalKey<NavigatorState>(),
    Tabs.Analytics: GlobalKey<NavigatorState>(),
    Tabs.Products: GlobalKey<NavigatorState>(),
  };

  void _selectTab(Tabs tabItem) {
    if (tabItem == _currentTab) {
      // pop to first route
      _navigatorKeys[tabItem]!.currentState!.popUntil((route) => route.isFirst);
    } else {
      setState(() => _currentTab = tabItem);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final isFirstRouteInCurrentTab =
            !await _navigatorKeys[_currentTab]!.currentState!.maybePop();
        if (isFirstRouteInCurrentTab) {
          // if not on the 'main' tab
          if (_currentTab != Tabs.Profile) {
            // select 'main' tab
            _selectTab(Tabs.Profile);
            // back button handled by app
            return false;
          }
        }
        // let system handle back button if we're on the first route
        return isFirstRouteInCurrentTab;
      },
      child: Scaffold(
        extendBody: true,
        body: SafeArea(
          bottom: false,
          child: Stack(fit: StackFit.expand, children: <Widget>[
            _buildOffstageNavigator(Tabs.Profile),
            _buildOffstageNavigator(Tabs.Analytics),
            _buildOffstageNavigator(Tabs.Products),
          ]),
        ),
        bottomNavigationBar: SellerBottomNavigation(
          currentTab: _currentTab,
          onSelectTab: _selectTab,
        ),
      ),
    );
  }

  Widget _buildOffstageNavigator(Tabs tabItem) {
    return Offstage(
      offstage: _currentTab != tabItem,
      child: TabNavigator(
        navigatorKey: _navigatorKeys[tabItem],
        tabItem: tabItem,
      ),
    );
  }
}

class SellerBottomNavigation extends StatelessWidget {
  SellerBottomNavigation({
    required this.currentTab,
    required this.onSelectTab,
  });
  final Tabs currentTab;
  final ValueChanged<Tabs> onSelectTab;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.05),
            blurRadius: 50.0,
            offset: Offset(0, -4),
          ),
        ],
      ),
      child: FittedBox(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            currentTab == Tabs.Profile
                ? Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          color: PRIMARY_COLOR.withOpacity(0.7), blurRadius: 25)
                    ]),
                    child: _button(Colors.white, PRIMARY_COLOR, Tabs.Profile,
                        Icons.person),
                  )
                : _button(Colors.black, const Color.fromRGBO(247, 250, 254, 1),
                    Tabs.Profile, Icons.person),
            const SizedBox(
              width: 25,
            ),
            currentTab == Tabs.Analytics
                ? Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          color: PRIMARY_COLOR.withOpacity(0.7), blurRadius: 25)
                    ]),
                    child: _button(Colors.white, PRIMARY_COLOR, Tabs.Analytics,
                        Icons.analytics),
                  )
                : _button(Colors.black, const Color.fromRGBO(247, 250, 254, 1),
                    Tabs.Analytics, Icons.analytics),
            const SizedBox(
              width: 25,
            ),
            currentTab == Tabs.Products
                ? Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          color: PRIMARY_COLOR.withOpacity(0.7), blurRadius: 25)
                    ]),
                    child: _button(
                        Colors.white, PRIMARY_COLOR, Tabs.Products, Icons.list),
                  )
                : _button(Colors.black, const Color.fromRGBO(247, 250, 254, 1),
                    Tabs.Products, Icons.list),
          ],
        ),
      ),
    );
  }

  Widget _button(Color iconColor, Color bgColor, Tabs tab, IconData icon) {
    return ElevatedButton(
      onPressed: () =>
          // context.read<NavigationCubit>().showHome(),
          onSelectTab(tab),
      child: Icon(icon, color: iconColor),
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(bgColor),
        shape: MaterialStateProperty.all(const CircleBorder()),
        padding: MaterialStateProperty.all(
          const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
        ),
        elevation: MaterialStateProperty.all(0),
      ),
    );
  }
}

class TabNavigatorRoutes {
  static const String root = '/';
}

class TabNavigator extends StatelessWidget {
  TabNavigator({required this.navigatorKey, required this.tabItem});
  final GlobalKey<NavigatorState>? navigatorKey;
  final Tabs tabItem;

  void _push(BuildContext context, {int materialIndex: 500}) {
    var routeBuilders = _routeBuilders(context, tabItem);
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, Tabs tab) {
    return {
      TabNavigatorRoutes.root: (context) {
        switch (tab) {
          case Tabs.Profile:
            return ProfileSellerScreen();

          case Tabs.Analytics:
            return AnalyticsSellerScreen();
          case Tabs.Products:
            return ProductsSellerScreen();
          default:
            return ProfileSellerScreen();
        }
      },
    };
  }

  @override
  Widget build(BuildContext context) {
    final routeBuilders = _routeBuilders(
      context,
      tabItem,
    );
    return Navigator(
      key: navigatorKey,
      initialRoute: TabNavigatorRoutes.root,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(
          builder: (context) => routeBuilders[routeSettings.name!]!(context),
        );
      },
    );
  }
}
