import 'package:dias_diplome/data/models/product_model.dart';
import 'package:dias_diplome/data/providers/api.dart';
import 'package:dias_diplome/data/providers/user_provider.dart';
import 'package:dias_diplome/data/repositories/products_repository.dart';
import 'package:dias_diplome/presentation/screens/seller/product_create_screen.dart';
import 'package:dias_diplome/presentation/screens/seller/product_seller_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductsSellerScreen extends StatefulWidget {
  const ProductsSellerScreen({Key? key}) : super(key: key);

  @override
  _ProductsSellerScreenState createState() => _ProductsSellerScreenState();
}

class _ProductsSellerScreenState extends State<ProductsSellerScreen> {
  late UserProvider _userProvider;
  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    return Scaffold(
      extendBody: true,
      body: Stack(
        fit: StackFit.expand,
        children: [
          _userProvider.userId == null
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : FutureBuilder(
                  future: ProductsRepository()
                      .fetchProducts(storeId: _userProvider.userId),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<Product>> snapshot) {
                    if (snapshot.hasData) {
                      if (snapshot.data!.length == 0) {
                        return Center(
                          child: Text('Список пуст'),
                        );
                      }
                      return GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10,
                          childAspectRatio: 121 / 210,
                        ),
                        itemCount: snapshot.data!.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(CupertinoPageRoute(
                                  builder: (_) => ProductSellerScreen(
                                        product: snapshot.data![index],
                                      )));
                            },
                            child: Container(
                              margin: EdgeInsets.all(30),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Image.network(
                                    '${snapshot.data![index].images!.first.src}',
                                    height: 110,
                                    fit: BoxFit.cover,
                                  ),
                                  SizedBox(height: 10),
                                  Text('${snapshot.data![index].name}'),
                                  SizedBox(height: 5),
                                  Text(
                                      '${snapshot.data![index].category!.name}'),
                                  SizedBox(height: 5),
                                  Text('${snapshot.data![index].price}'),
                                  SizedBox(height: 5),
                                  Text(
                                      '${snapshot.data![index].colors!.first.name}'),
                                  SizedBox(height: 5),
                                  Text('${snapshot.data![index].size!.name}'),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    } else if (snapshot.hasError) {
                      return Center(
                        child: Text('Ошибка'),
                      );
                    }
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  },
                ),
          Positioned(
            left: 30,
            right: 30,
            bottom: 130,
            child: ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(
                    CupertinoPageRoute(builder: (_) => ProductCreateScreen()));
              },
              child: Text('Создать товар'),
            ),
          )
        ],
      ),
    );
  }
}
