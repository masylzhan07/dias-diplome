import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/models/basket_model.dart';
import 'package:dias_diplome/data/models/product_model.dart';
import 'package:dias_diplome/data/providers/api.dart';
import 'package:dias_diplome/data/providers/user_provider.dart';
import 'package:dias_diplome/data/repositories/basket_repository.dart';
import 'package:dias_diplome/data/repositories/products_repository.dart';
import 'package:dias_diplome/main.dart';
import 'package:dias_diplome/presentation/screens/authorization_screen.dart';
import 'package:dias_diplome/presentation/screens/order_screen.dart';
import 'package:dias_diplome/presentation/screens/seller/product_seller_screen.dart';
import 'package:dias_diplome/presentation/widgets/app_navigator.dart';
import 'package:dias_diplome/presentation/widgets/auth_navigator.dart';
import 'package:dias_diplome/presentation/widgets/loading_overlay.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class BasketScreen extends StatefulWidget {
  final int? categoryId;
  final int? storeId;

  const BasketScreen({Key? key, this.storeId, this.categoryId})
      : super(key: key);

  @override
  _BasketScreenState createState() => _BasketScreenState();
}

class _BasketScreenState extends State<BasketScreen> {
  late UserProvider _userProvider;
  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    print('AAA');
    print(_userProvider.deviceId);
    return Scaffold(
      extendBody: true,
      body: FutureBuilder(
        future: BasketRepository()
            .fetchBasketProducts(_userProvider.userId, _userProvider.deviceId),
        builder: (BuildContext context, AsyncSnapshot<BasketModel> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data!.products.length == 0) {
              return Center(
                child: Text('Список пуст'),
              );
            }
            int totalCount = snapshot.data!.products.fold(
                0,
                (previousValue, element) =>
                    previousValue + element.pivot!.count!);
            double totalPrice = snapshot.data!.products.fold(
                0,
                (previousValue, element) =>
                    previousValue +
                    (double.parse('${element.price}') * element.pivot!.count!));

            return Stack(
              fit: StackFit.expand,
              children: [
                GridView.builder(
                  padding: EdgeInsets.only(bottom: 150),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    childAspectRatio: 121 / 250,
                  ),
                  itemCount: snapshot.data!.products.length,
                  itemBuilder: (BuildContext context, int index) {
                    totalCount += snapshot.data!.products[index].pivot!.count!;
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(CupertinoPageRoute(
                            builder: (_) => ProductSellerScreen(
                                  product: snapshot.data!.products[index],
                                )));
                      },
                      child: Container(
                        margin: EdgeInsets.all(30),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.network(
                              '${snapshot.data!.products[index].images!.first.src}',
                              height: 110,
                              fit: BoxFit.cover,
                            ),
                            SizedBox(height: 10),
                            Text('${snapshot.data!.products[index].name}'),
                            SizedBox(height: 5),
                            Text(
                                '${snapshot.data!.products[index].category!.name}'),
                            SizedBox(height: 5),
                            Text('${snapshot.data!.products[index].price}'),
                            SizedBox(height: 5),
                            if (snapshot
                                .data!.products[index].colors!.isNotEmpty)
                              Padding(
                                padding: const EdgeInsets.only(bottom: 5),
                                child: Text(
                                    '${snapshot.data!.products[index].colors!.first.name}'),
                              ),
                            Text(
                                '${snapshot.data!.products[index].size!.name}'),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                IconButton(
                                  splashRadius: 24,
                                  onPressed: () async {
                                    const FlutterSecureStorage _storage =
                                        FlutterSecureStorage();
                                    String? token =
                                        await _storage.read(key: 'accessToken');
                                    token = _userProvider.deviceId;
                                    final overlay = LoadingOverlay.of(context);

                                    final response = await overlay
                                        .during(
                                      APIRepository()
                                          .changeProductCountInBasket(
                                        snapshot.data!.id!,
                                        // FormData.fromMap(
                                        {
                                          'product_id':
                                              snapshot.data!.products[index].id,
                                          'filter[]': [],
                                          'filter[app_number]': token,
                                          'count': snapshot
                                                  .data!
                                                  .products[index]
                                                  .pivot!
                                                  .count! -
                                              1
                                        },
                                        // ),
                                      ),
                                    )
                                        .then((value) {
                                      setState(() {});
                                    });
                                  },
                                  icon: Icon(Icons.remove),
                                ),
                                Text(
                                    '${snapshot.data!.products[index].pivot!.count}'),
                                IconButton(
                                  splashRadius: 24,
                                  onPressed: () async {
                                    String token = _userProvider.deviceId!;
                                    final overlay = LoadingOverlay.of(context);
                                    final response = await overlay
                                        .during(
                                      APIRepository()
                                          .changeProductCountInBasket(
                                        snapshot.data!.id!,
                                        // FormData.fromMap(
                                        {
                                          'product_id':
                                              snapshot.data!.products[index].id,
                                          'filter[]': [],
                                          'filter[app_number]': token,
                                          'count': snapshot
                                                  .data!
                                                  .products[index]
                                                  .pivot!
                                                  .count! +
                                              1
                                        },
                                        // ),
                                      ),
                                    )
                                        .then((value) {
                                      setState(() {});
                                    });
                                  },
                                  icon: Icon(Icons.add),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
                Positioned(
                  bottom: 85,
                  child: Container(
                    color: PRIMARY_COLOR,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    child: Column(
                      children: [
                        FittedBox(
                          child: Row(
                            children: [
                              Text.rich(
                                TextSpan(
                                  children: [
                                    const TextSpan(
                                      text: 'Общее количество товаров: ',
                                    ),
                                    TextSpan(
                                      text: '$totalCount',
                                      style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(width: 20),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    const TextSpan(
                                      text: 'Общая сумма: ',
                                    ),
                                    TextSpan(
                                      text: '$totalPrice',
                                      style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        TextButton(
                          onPressed: () {
                            if (_userProvider.roleId == null) {
                              showDialog(
                                context: context,
                                builder: (ctxAlert) => AlertDialog(
                                  content: Text(
                                      'Нужно авторизоваться для оформления заказа'),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(ctxAlert).pop();
                                      },
                                      child: Text('Отменить'),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(ctxAlert).pop();
                                        Navigator.of(context,
                                                rootNavigator: true)
                                            .pushReplacement(CupertinoPageRoute(
                                                builder: (_) => MyApp()));
                                      },
                                      child: Text('Авторизоваться'),
                                    )
                                  ],
                                ),
                              );
                            } else {
                              Navigator.of(context).push(CupertinoPageRoute(
                                  builder: (_) => OrderScreen()));
                            }
                            // APIRepository().getOrders().then(
                            //   (value) async {
                            // print(value.data['data']['hash']);
                            // launch(Uri.https(
                            //   'epay.kkb.kz',
                            //   '/jsp/client/pay.jsp',
                            //   {
                            //     'Base64Content': value.data['data']['hash'],
                            //     'Language': 'rus',
                            //     'PostLink':
                            //         'http://epay.kkb.kz/jsp/client/pl.jsp',
                            //   },
                            // ).toString());
                            // APIRepository().payForm(
                            //   FormData.fromMap(
                            //     {
                            // 'Base64Content': value.data['hash'],
                            // 'Language': 'rus',
                            // 'PostLink':
                            //     'http://epay.kkb.kz/jsp/client/pl.jsp',
                            //     },
                            //   ),
                            // );
                            //   },
                            // );
                          },
                          child: const Text(
                            'Оформить заказ',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Ошибка'),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
