import 'package:dias_diplome/data/models/user_model.dart';
import 'package:dias_diplome/data/providers/user_provider.dart';
import 'package:dias_diplome/data/repositories/profile_repository.dart';
import 'package:dias_diplome/presentation/screens/customer/bottom_navbar_customer.dart';
import 'package:dias_diplome/presentation/screens/seller/bottom_navbar_seller.dart';
import 'package:dias_diplome/presentation/widgets/bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UserLoaderScreen extends StatefulWidget {
  const UserLoaderScreen({Key? key}) : super(key: key);

  @override
  _UserLoaderScreenState createState() => _UserLoaderScreenState();
}

class _UserLoaderScreenState extends State<UserLoaderScreen> {
  bool _isHandleRequest = true;
  bool _isHandleRequestError = false;
  late User _currentUser;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadData();
  }

  void _loadData() async {
    try {
      final response = await ProfileRepository().fetchProfile();
      Provider.of<UserProvider>(context, listen: false)
          .setRoleId(response.role!.id!);
      setState(() {
        _currentUser = response;
        _isHandleRequest = false;
      });
    } on Exception catch (e) {
      setState(() {
        _isHandleRequest = false;
        _isHandleRequestError = true;
      });
      throw Exception(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_isHandleRequest) {
      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    if (_isHandleRequestError) {
      return Scaffold(
        body: Center(
          child: Text('Ошибка'),
        ),
      );
    } else {
      if (_currentUser.role!.id == 102) {
        return const BottomNavbarSeller();
      }
      if (_currentUser.role!.id == 103) {
        return const BottomNavbarCustomer();
      }
      return const CustomBottomNavigation();
    }
  }
}
