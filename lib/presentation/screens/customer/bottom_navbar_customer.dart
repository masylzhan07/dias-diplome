import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/models/user_model.dart';
import 'package:dias_diplome/data/providers/api.dart';
import 'package:dias_diplome/data/providers/user_provider.dart';
import 'package:dias_diplome/data/repositories/profile_repository.dart';
import 'package:dias_diplome/logic/device_access.dart';
import 'package:dias_diplome/presentation/screens/basket_screen.dart';
import 'package:dias_diplome/presentation/screens/categories_screen.dart';
import 'package:dias_diplome/presentation/screens/customer/profile_customer_screen.dart';
import 'package:dias_diplome/presentation/screens/stores_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

enum Tabs { Stores, Categories, Basket, Profile }

class BottomNavbarCustomer extends StatefulWidget {
  static const routeName = '/bottom-nav-customer';

  const BottomNavbarCustomer({
    Key? key,
  }) : super(key: key);

  @override
  _BottomNavbarCustomerState createState() => _BottomNavbarCustomerState();
}

class _BottomNavbarCustomerState extends State<BottomNavbarCustomer> {
  var _currentTab = Tabs.Stores;
  final _navigatorKeys = {
    Tabs.Stores: GlobalKey<NavigatorState>(),
    Tabs.Categories: GlobalKey<NavigatorState>(),
    Tabs.Basket: GlobalKey<NavigatorState>(),
    Tabs.Profile: GlobalKey<NavigatorState>(),
  };

  void _selectTab(Tabs tabItem) {
    if (tabItem == _currentTab) {
      // pop to first route
      _navigatorKeys[tabItem]!.currentState!.popUntil((route) => route.isFirst);
    } else {
      setState(() => _currentTab = tabItem);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final isFirstRouteInCurrentTab =
            !await _navigatorKeys[_currentTab]!.currentState!.maybePop();
        if (isFirstRouteInCurrentTab) {
          // if not on the 'main' tab
          if (_currentTab != Tabs.Stores) {
            // select 'main' tab
            _selectTab(Tabs.Stores);
            // back button handled by app
            return false;
          }
        }
        // let system handle back button if we're on the first route
        return isFirstRouteInCurrentTab;
      },
      child: Scaffold(
        extendBody: true,
        body: SafeArea(
          bottom: false,
          child: Stack(fit: StackFit.expand, children: <Widget>[
            _buildOffstageNavigator(Tabs.Stores),
            _buildOffstageNavigator(Tabs.Categories),
            _buildOffstageNavigator(Tabs.Basket),
            _buildOffstageNavigator(Tabs.Profile),
          ]),
        ),
        bottomNavigationBar: BottomNavigation(
          currentTab: _currentTab,
          onSelectTab: _selectTab,
        ),
      ),
    );
  }

  Widget _buildOffstageNavigator(Tabs tabItem) {
    return Offstage(
      offstage: _currentTab != tabItem,
      child: TabNavigator(
        navigatorKey: _navigatorKeys[tabItem],
        tabItem: tabItem,
      ),
    );
  }
}

class BottomNavigation extends StatelessWidget {
  BottomNavigation({
    required this.currentTab,
    required this.onSelectTab,
  });
  final Tabs currentTab;
  final ValueChanged<Tabs> onSelectTab;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.05),
            blurRadius: 50.0,
            offset: Offset(0, -4),
          ),
        ],
      ),
      child: FittedBox(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            currentTab == Tabs.Stores
                ? Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          color: PRIMARY_COLOR.withOpacity(0.7), blurRadius: 25)
                    ]),
                    child: _button(
                        Colors.white, PRIMARY_COLOR, Tabs.Stores, Icons.store),
                  )
                : _button(Colors.black, const Color.fromRGBO(247, 250, 254, 1),
                    Tabs.Stores, Icons.store),
            const SizedBox(
              width: 25,
            ),
            currentTab == Tabs.Categories
                ? Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          color: PRIMARY_COLOR.withOpacity(0.7), blurRadius: 25)
                    ]),
                    child: _button(Colors.white, PRIMARY_COLOR, Tabs.Categories,
                        Icons.list),
                  )
                : _button(Colors.black, const Color.fromRGBO(247, 250, 254, 1),
                    Tabs.Categories, Icons.list),
            const SizedBox(
              width: 25,
            ),
            currentTab == Tabs.Basket
                ? Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          color: PRIMARY_COLOR.withOpacity(0.7), blurRadius: 25)
                    ]),
                    child: _button(Colors.white, PRIMARY_COLOR, Tabs.Basket,
                        Icons.shopping_basket),
                  )
                : _button(Colors.black, const Color.fromRGBO(247, 250, 254, 1),
                    Tabs.Basket, Icons.shopping_basket),
            const SizedBox(
              width: 25,
            ),
            currentTab == Tabs.Profile
                ? Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          color: PRIMARY_COLOR.withOpacity(0.7), blurRadius: 25)
                    ]),
                    child: _button(Colors.white, PRIMARY_COLOR, Tabs.Profile,
                        Icons.person),
                  )
                : _button(Colors.black, const Color.fromRGBO(247, 250, 254, 1),
                    Tabs.Profile, Icons.person),
          ],
        ),
      ),
    );
  }

  Widget _button(Color iconColor, Color bgColor, Tabs tab, IconData icon) {
    return ElevatedButton(
      onPressed: () =>
          // context.read<NavigationCubit>().showHome(),
          onSelectTab(tab),
      child: Icon(icon, color: iconColor),
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(bgColor),
        shape: MaterialStateProperty.all(const CircleBorder()),
        padding: MaterialStateProperty.all(
          const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
        ),
        elevation: MaterialStateProperty.all(0),
      ),
    );
  }
}

class TabNavigatorRoutes {
  static const String root = '/';
}

class TabNavigator extends StatelessWidget {
  TabNavigator({required this.navigatorKey, required this.tabItem});
  final GlobalKey<NavigatorState>? navigatorKey;
  final Tabs tabItem;

  void _push(BuildContext context, {int materialIndex: 500}) {
    var routeBuilders = _routeBuilders(context, tabItem);
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, Tabs tab) {
    return {
      TabNavigatorRoutes.root: (context) {
        switch (tab) {
          case Tabs.Stores:
            return StoresScreen();

          case Tabs.Categories:
            return CategoriesScreen();
          case Tabs.Basket:
            return BasketScreen();
          case Tabs.Profile:
            return ProfileCustomerScreen();
          default:
            return StoresScreen();
        }
      },
    };
  }

  @override
  Widget build(BuildContext context) {
    final routeBuilders = _routeBuilders(
      context,
      tabItem,
    );
    return Navigator(
      key: navigatorKey,
      initialRoute: TabNavigatorRoutes.root,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(
          builder: (context) => routeBuilders[routeSettings.name!]!(context),
        );
      },
    );
  }
}

class CustomBottomNavShape extends ShapeBorder {
  const CustomBottomNavShape();

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.zero;

  @override
  Path getInnerPath(Rect rect, {TextDirection? textDirection}) => Path();

  // double holeSize = 70;

  @override
  Path getOuterPath(Rect rect, {TextDirection? textDirection}) {
    // print(rect.height);
    return Path()
      ..addRRect(RRect.fromRectAndRadius(rect, const Radius.circular(50)))
      ..close();
    // return Path.combine(
    //   PathOperation.difference,
    //   Path()
    //     ..addRRect(
    //         RRect.fromRectAndRadius(rect, Radius.circular(rect.height / 2)))
    //     ..close(),
    //   Path()
    //     ..addOval(Rect.fromCenter(
    //         center: rect.center.translate(0, -rect.height / 2),
    //         height: holeSize,
    //         width: holeSize))
    //     ..close(),
    // );
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection? textDirection}) {}

  @override
  ShapeBorder scale(double t) => this;
}
