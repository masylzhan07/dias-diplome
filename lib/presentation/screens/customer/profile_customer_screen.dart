import 'dart:io';

import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/models/seller_model.dart';
import 'package:dias_diplome/data/models/user_model.dart';
import 'package:dias_diplome/data/providers/api.dart';
import 'package:dias_diplome/data/repositories/profile_repository.dart';
import 'package:dias_diplome/logic/cubit/session_logic/session_cubit.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

class ProfileCustomerScreen extends StatefulWidget {
  const ProfileCustomerScreen({Key? key}) : super(key: key);

  @override
  _ProfileCustomerScreenState createState() => _ProfileCustomerScreenState();
}

class _ProfileCustomerScreenState extends State<ProfileCustomerScreen> {
  void _editFunc(User user) async {
    await showDialog(
        context: context,
        builder: (_) {
          return UpdateProfileDialog(user: user);
        }).then((value) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: ProfileRepository().fetchProfile(),
      builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
        if (snapshot.hasData) {
          List _list = [
            {
              'name': snapshot.data!.name,
              'icon': Icons.person,
            },
            {
              'name': snapshot.data!.email,
              'icon': Icons.email,
            },
            {
              'name': snapshot.data!.phone,
              'icon': Icons.phone,
            },
            {
              'name': snapshot.data!.address_of_shop,
              'icon': Icons.location_city,
            },
          ];
          return Scaffold(
            extendBody: true,
            appBar:
                CustomProfileAppBar(user: snapshot.data!, editFunc: _editFunc),
            body: Padding(
              padding: const EdgeInsets.only(top: 50),
              child: ListView.separated(
                padding:
                    EdgeInsets.only(top: 30, bottom: 130, left: 30, right: 30),
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.05),
                          offset: Offset(0, 4),
                          blurRadius: 30,
                        ),
                      ],
                    ),
                    child: Row(
                      children: [
                        Icon(
                          _list[index]['icon'],
                          color: PRIMARY_COLOR,
                        ),
                        SizedBox(width: 20),
                        Text('${_list[index]['name']}')
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return SizedBox(height: 20);
                },
                itemCount: _list.length,
              ),
            ),
          );
        } else if (snapshot.hasError) {
          return Scaffold(
            extendBody: true,
            body: Center(
              child: Text('Ошибка'),
            ),
          );
        }
        return Scaffold(
          extendBody: true,
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }
}

class CustomProfileAppBar extends StatelessWidget with PreferredSizeWidget {
  final User user;
  final Function editFunc;
  const CustomProfileAppBar({
    Key? key,
    required this.user,
    required this.editFunc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            PRIMARY_COLOR,
            Color.fromRGBO(130, 190, 225, 1),
          ],
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
        ),
      ),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          IconButton(
            onPressed: () => editFunc(user),
            splashRadius: 5,
            icon: Icon(
              Icons.edit,
              color: Colors.white,
            ),
          ),
          Container(
            transform: Matrix4.translationValues(0, 50, 0),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  width: 3,
                  color: Colors.white,
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.2),
                    offset: Offset(0, 4),
                    blurRadius: 30,
                  ),
                ]),
            child: Image.asset(
              'assets/images/avatar.png',
              width: 100,
              height: 100,
            ),
          ),
          IconButton(
            onPressed: () {
              BlocProvider.of<SessionCubit>(context).signOut();
            },
            splashRadius: 5,
            icon: Icon(
              Icons.exit_to_app,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(200);
}

class UpdateProfileDialog extends StatefulWidget {
  final User user;
  const UpdateProfileDialog({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  State<UpdateProfileDialog> createState() => _UpdateProfileDialogState();
}

class _UpdateProfileDialogState extends State<UpdateProfileDialog> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      title: const Text('Редактирование профиля'),
      content: Form(
        key: GlobalKey<FormState>(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextFormField(
              controller: _nameController,
              decoration: const InputDecoration(
                hintText: 'Имя',
                isDense: true,
                contentPadding: EdgeInsets.all(20),
                filled: true,
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
              ),
            ),
            const SizedBox(height: 15),
            TextFormField(
              controller: _phoneController,
              decoration: const InputDecoration(
                hintText: 'Номер',
                isDense: true,
                contentPadding: EdgeInsets.all(20),
                filled: true,
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
              ),
            ),
            const SizedBox(height: 15),
            TextFormField(
              controller: _addressController,
              decoration: const InputDecoration(
                hintText: 'Адрес',
                isDense: true,
                contentPadding: EdgeInsets.all(20),
                filled: true,
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(239, 246, 255, 1), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
              ),
            ),
            const SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Отменить',
                  ),
                ),
                SizedBox(width: 10),
                ElevatedButton(
                  onPressed: () async {
                    await APIRepository().updateProfile(
                      widget.user.id,
                      {
                        'name': _nameController.text,
                        'address_of_shop': _addressController.text,
                        'phone': _phoneController.text,
                      },
                      // FormData.fromMap({
                      //   'background_image_of_shop': _image,
                      // }),
                    ).then((value) {
                      Navigator.of(context).pop(value.statusCode == 200);
                    });
                  },
                  child: Text(
                    'Обновить',
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
