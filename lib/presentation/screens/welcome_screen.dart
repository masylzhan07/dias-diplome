import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/providers/user_provider.dart';
import 'package:dias_diplome/data/repositories/auth_repository.dart';
import 'package:dias_diplome/logic/cubit/session_logic/session_cubit.dart';
import 'package:dias_diplome/presentation/widgets/app_navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3)).then(
        (value) => Navigator.of(context).pushReplacement(CupertinoPageRoute(
              builder: (_) => MultiRepositoryProvider(
                providers: [
                  RepositoryProvider(create: (context) => AuthRepository()),
                  // RepositoryProvider(create: (context) => DataRepository())
                ],
                child: MultiProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => SessionCubit(
                        authRepo: context.read<AuthRepository>(),
                        // dataRepo: context.read<DataRepository>(),
                      ),
                    ),
                    ChangeNotifierProvider(create: (context) => UserProvider()),

                    // BlocProvider(create: (context) => ProfileBloc()),
                  ],
                  child: AppNavigator(),
                ),
              ),
            )));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PRIMARY_COLOR,
      body: Center(child: Lottie.asset('assets/images/shopping-gif.json')),
    );
  }
}
