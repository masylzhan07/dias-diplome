import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/models/role_model.dart';
import 'package:dias_diplome/data/repositories/auth_repository.dart';
import 'package:dias_diplome/logic/bloc/signup_logic/sign_up_bloc.dart';
import 'package:dias_diplome/logic/bloc/signup_logic/sign_up_event.dart';
import 'package:dias_diplome/logic/bloc/signup_logic/sign_up_state.dart';
import 'package:dias_diplome/logic/cubit/auth_logic/auth_cubit.dart';
import 'package:dias_diplome/presentation/widgets/form_submission_status.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignUpView extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      backgroundColor: PRIMARY_COLOR,
      body: BlocProvider(
        create: (context) => SignUpBloc(
          authRepo: context.read<AuthRepository>(),
          authCubit: context.read<AuthCubit>(),
        ),
        child: Container(
          margin: const EdgeInsets.only(top: 40),
          padding: const EdgeInsets.symmetric(horizontal: 30),
          alignment: Alignment.center,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _signUpForm(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextButton(
                      onPressed: () =>
                          context.read<AuthCubit>().sessionCubit!.logAsGuest(),
                      style: const ButtonStyle(alignment: Alignment.center),
                      child: const Text(
                        'Войти как гость',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    _showLoginButton(context),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _signUpForm() {
    return BlocListener<SignUpBloc, SignUpState>(
        listener: (context, state) {
          final formStatus = state.formStatus;
          if (formStatus is SubmissionFailed) {
            _showSnackBar(context, formStatus.exception.toString());
          }
        },
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _nameField(),
              // _surnameField(),
              const SizedBox(height: 10),
              _emailField(),
              const SizedBox(height: 10),
              // _phoneField(),
              _passwordField(),
              const SizedBox(height: 10),
              _roleField(),
              const SizedBox(height: 10),
              _signUpButton(),
            ],
          ),
        ));
  }

  Widget _nameField() {
    return BlocBuilder<SignUpBloc, SignUpState>(builder: (context, state) {
      return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 4),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.03),
              blurRadius: 4,
              offset: Offset(0, 4),
            ),
          ],
        ),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: Icon(Icons.person),
            hintText: 'Имя',
            border: InputBorder.none,
          ),
          onChanged: (value) => context.read<SignUpBloc>().add(
                SignUpNameChanged(name: value),
              ),
        ),
      );
    });
  }

  Widget _surnameField() {
    return BlocBuilder<SignUpBloc, SignUpState>(builder: (context, state) {
      return TextFormField(
        decoration: InputDecoration(
          icon: Icon(Icons.person),
          hintText: 'Surname',
        ),
        // validator: (value) =>
        //     state.isValidUsername ? null : 'Username is too short',
        onChanged: (value) => context.read<SignUpBloc>().add(
              SignUpSurnameChanged(surname: value),
            ),
      );
    });
  }

  Widget _roleField() {
    List<Role> _roles = [
      Role(id: 102, name: 'Продавец', slug: 'seller'),
      Role(id: 103, name: 'Гость', slug: 'guest')
    ];
    return BlocBuilder<SignUpBloc, SignUpState>(builder: (context, state) {
      return DropdownButton<int>(
        value: state.roleId,
        icon: Container(
          margin: const EdgeInsets.only(left: 10),
          child: const Icon(Icons.keyboard_arrow_down),
        ),
        iconEnabledColor: const Color.fromRGBO(255, 255, 255, 1),
        dropdownColor: PRIMARY_COLOR,
        underline: const SizedBox(),
        onChanged: (int? value) async {
          context.read<SignUpBloc>().add(SignUpRoleChanged(roleId: value));
        },
        items: _roles
            .map((value) => DropdownMenuItem<int>(
                  value: value.id,
                  child: Text(
                    '${value.name}',
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ))
            .toList(),
      );
    });
  }

  Widget _emailField() {
    return BlocBuilder<SignUpBloc, SignUpState>(builder: (context, state) {
      return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 4),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.03),
              blurRadius: 4,
              offset: Offset(0, 4),
            ),
          ],
        ),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: Icon(Icons.email),
            hintText: 'Email',
            border: InputBorder.none,
          ),
          onChanged: (value) => context.read<SignUpBloc>().add(
                SignUpEmailChanged(email: value),
              ),
        ),
      );
    });
  }

  Widget _phoneField() {
    return BlocBuilder<SignUpBloc, SignUpState>(builder: (context, state) {
      return TextFormField(
        decoration: InputDecoration(
          icon: Icon(Icons.phone),
          hintText: 'Phone',
        ),
        // validator: (value) => state.isValidUsername ? null : 'Invalid email',
        onChanged: (value) => context.read<SignUpBloc>().add(
              SignUpPhoneChanged(phone: value),
            ),
      );
    });
  }

  Widget _passwordField() {
    return BlocBuilder<SignUpBloc, SignUpState>(builder: (context, state) {
      return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 4),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.03),
              blurRadius: 4,
              offset: Offset(0, 4),
            ),
          ],
        ),
        child: TextFormField(
          obscureText: true,
          decoration: const InputDecoration(
            icon: Icon(Icons.security),
            hintText: 'Пароль',
            border: InputBorder.none,
            errorMaxLines: 3,
          ),
          validator: (value) => state.isValidPassword
              ? null
              : 'Пароль должен содежать минимум 8 символов',
          onChanged: (value) => context.read<SignUpBloc>().add(
                SignUpPasswordChanged(password: value),
              ),
        ),
      );
    });
  }

  Widget _signUpButton() {
    return BlocBuilder<SignUpBloc, SignUpState>(builder: (context, state) {
      return state.formStatus is FormSubmitting
          ? CircularProgressIndicator()
          : OutlinedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  context.read<SignUpBloc>().add(SignUpSubmitted());
                }
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.white),
              ),
              child: const Text(
                'Зарегистрироваться',
                style: TextStyle(
                  color: PRIMARY_COLOR,
                ),
              ),
            );
    });
  }

  Widget _showLoginButton(BuildContext context) {
    return TextButton(
      onPressed: () => context.read<AuthCubit>().showLogin(),
      style: const ButtonStyle(alignment: Alignment.center),
      child: const Text(
        'Авторизация',
        style: TextStyle(
          color: Colors.white,
          fontSize: 14,
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
