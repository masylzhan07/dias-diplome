import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/models/category_model.dart';
import 'package:dias_diplome/data/repositories/categories_repository.dart';
import 'package:dias_diplome/presentation/screens/products_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoriesScreen extends StatelessWidget {
  final int? parentId;
  const CategoriesScreen({Key? key, this.parentId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: FutureBuilder(
          future: CategoriesRepository().fetchCategories(parentId: parentId),
          builder:
              (BuildContext context, AsyncSnapshot<List<Category>> snapshot) {
            if (snapshot.hasData) {
              return CategoriesList(catList: snapshot.data ?? []);
            } else if (snapshot.hasError) {
              return Center(
                child: Text('Error'),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }
}

class CategoriesList extends StatelessWidget {
  final List<Category> catList;
  const CategoriesList({
    Key? key,
    required this.catList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: EdgeInsets.only(top: 20, bottom: 130, left: 30, right: 30),
        itemCount: catList.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              if (catList[index].childs != null) {
                Navigator.of(context).push(
                  CupertinoPageRoute(
                    builder: (_) => CategoriesScreen(
                      parentId: catList[index].id,
                    ),
                  ),
                );
              } else {
                Navigator.of(context).push(
                  CupertinoPageRoute(
                    builder: (_) => ProductsScreen(
                      categoryId: catList[index].id,
                    ),
                  ),
                );
              }
            },
            child: Container(
              margin: EdgeInsets.only(bottom: 10),
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              color: PRIMARY_COLOR,
              child: Text(
                '${catList[index].name}',
                style: TextStyle(color: Colors.white),
              ),
            ),
          );
        });
  }
}
