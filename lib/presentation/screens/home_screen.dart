import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeScreen extends StatelessWidget {
  static const routeName = '/home';
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      displacement: 0,
      edgeOffset: 0,
      onRefresh: () {
        return Future.delayed(Duration(seconds: 1));
      },
      child: SingleChildScrollView(
          // child: Container(
          //   padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 25),
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: [
          //       FittedBox(
          //         child: Row(
          //           children: [
          //             Text(
          //               'Маршруты',
          //               // style: const TextStyle()
          //               //     .merge(CustomTextStyle.largeTitleFontStyle),
          //             ),
          //             const SizedBox(width: 15),
          //             SvgPicture.asset('assets/icons/arrow-right.svg')
          //           ],
          //         ),
          //       ),
          //       const SizedBox(height: 25),
          //       StreamBuilder<RoutesModel>(
          //         stream: RoutesRepository.fetchRoutesList(),
          //         initialData: RoutesModel(),
          //         builder: (BuildContext context,
          //             AsyncSnapshot<RoutesModel> snapshot) {
          //           if (snapshot.connectionState == ConnectionState.waiting) {
          //             return Column(
          //               children: [Center(child: CircularProgressIndicator())],
          //             );
          //           } else if (snapshot.hasError) {
          //             print(snapshot.error);
          //             return Column(
          //               children: [
          //                 Center(child: Text(snapshot.error.toString()))
          //               ],
          //             );
          //           }

          //           // if (snapshot.data.items.length <= 0) {
          //           //   return Column(
          //           //     children: [
          //           //       Center(
          //           //         child: Text("Список гостей пуст".localized(context)),
          //           //       )
          //           //     ],
          //           //   );
          //           // }
          //           return GridView.builder(
          //             shrinkWrap: true,
          //             physics: const NeverScrollableScrollPhysics(),
          //             gridDelegate:
          //                 const SliverGridDelegateWithMaxCrossAxisExtent(
          //               maxCrossAxisExtent: 168,
          //               childAspectRatio: 168 / 296,
          //               crossAxisSpacing: 15,
          //               mainAxisSpacing: 25,
          //             ),
          //             itemCount: snapshot.data?.items?.length ?? 0,
          //             itemBuilder: (BuildContext context, int index) {
          //               RouteModel? routeCard = snapshot.data?.items?[index];
          //               return Column(
          //                 crossAxisAlignment: CrossAxisAlignment.start,
          //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                 children: [
          //                   AspectRatio(
          //                     aspectRatio: 168 / 234,
          //                     child: Stack(
          //                       fit: StackFit.expand,
          //                       children: [
          //                         ClipRRect(
          //                           borderRadius: BorderRadius.circular(10),
          //                           child: Image.asset(
          //                             routeCard!.images!.first,
          //                             fit: BoxFit.cover,
          //                           ),
          //                         ),
          //                         Positioned.fill(
          //                           child: Container(
          //                             decoration: BoxDecoration(
          //                               borderRadius: BorderRadius.circular(10),
          //                               gradient: const LinearGradient(
          //                                 colors: [
          //                                   Color.fromRGBO(0, 0, 0, 0),
          //                                   Color.fromRGBO(0, 0, 0, 0.63)
          //                                 ],
          //                                 begin: Alignment.topCenter,
          //                                 end: Alignment.bottomCenter,
          //                               ),
          //                             ),
          //                           ),
          //                         ),
          //                         Positioned(
          //                           left: 30,
          //                           right: 30,
          //                           bottom: 30,
          //                           child: Column(
          //                             mainAxisAlignment: MainAxisAlignment.end,
          //                             children: [
          //                               Row(
          //                                 children: [
          //                                   SvgPicture.asset(
          //                                       'assets/icons/star.svg',
          //                                       color: GOLDEN_COLOR),
          //                                   const SizedBox(
          //                                     width: 5,
          //                                   ),
          //                                   Text(
          //                                     '${routeCard.rating}',
          //                                     style: TextStyle().merge(
          //                                         CustomTextStyle
          //                                             .ratingFontStyle),
          //                                   ),
          //                                 ],
          //                               ),
          //                               Text(
          //                                 '${routeCard.feedbacksCount} отзывов',
          //                                 style: TextStyle(color: Colors.white)
          //                                     .merge(CustomTextStyle
          //                                         .feedbackCountFontStyle),
          //                               ),
          //                             ],
          //                           ),
          //                         ),
          //                       ],
          //                     ),
          //                   ),
          //                   AspectRatio(
          //                       aspectRatio: 168 / 47,
          //                       child: FittedBox(
          //                         child: Column(
          //                           children: [
          //                             Text(
          //                               '${routeCard.name}',
          //                               style: const TextStyle().merge(
          //                                   CustomTextStyle.normalTitleFontStyle),
          //                             ),
          //                             const SizedBox(height: 10),
          //                             Row(
          //                               children: [
          //                                 SvgPicture.asset(
          //                                   'assets/icons/map-pin.svg',
          //                                   width: 20,
          //                                   height: 20,
          //                                   color: GOLDEN_COLOR,
          //                                 ),
          //                                 const SizedBox(width: 5),
          //                                 Text(
          //                                   '${routeCard.region}',
          //                                   style: const TextStyle().merge(
          //                                       CustomTextStyle.regionFontStyle),
          //                                 ),
          //                               ],
          //                             ),
          //                           ],
          //                         ),
          //                       )),
          //                 ],
          //               );
          //             },
          //           );
          //         },
          //       ),
          //       FittedBox(
          //         child: Row(
          //           children: [
          //             Text(
          //               'Гиды',
          //               style: const TextStyle()
          //                   .merge(CustomTextStyle.largeTitleFontStyle),
          //             ),
          //             const SizedBox(width: 15),
          //             SvgPicture.asset('assets/icons/arrow-right.svg')
          //           ],
          //         ),
          //       ),
          //       const SizedBox(height: 25),
          //       StreamBuilder<GuidesModel>(
          //         stream: GuidesRepository.fetchGuides(),
          //         initialData: GuidesModel(),
          //         builder: (BuildContext context,
          //             AsyncSnapshot<GuidesModel> snapshot) {
          //           if (snapshot.connectionState == ConnectionState.waiting) {
          //             return Column(
          //               children: [Center(child: CircularProgressIndicator())],
          //             );
          //           } else if (snapshot.hasError) {
          //             print(snapshot.error);
          //             return Column(
          //               children: [
          //                 Center(child: Text(snapshot.error.toString()))
          //               ],
          //             );
          //           }

          //           // if (snapshot.data.items.length <= 0) {
          //           //   return Column(
          //           //     children: [
          //           //       Center(
          //           //         child: Text("Список гостей пуст".localized(context)),
          //           //       )
          //           //     ],
          //           //   );
          //           // }
          //           return ListView.builder(
          //               shrinkWrap: true,
          //               physics: const NeverScrollableScrollPhysics(),
          //               itemCount: snapshot.data?.items?.length ?? 0,
          //               itemBuilder: (BuildContext context, int index) {
          //                 GuideModel? guideCard = snapshot.data?.items?[index];

          //                 return Container(
          //                   margin: EdgeInsets.symmetric(vertical: 7.5),
          //                   padding: EdgeInsets.all(30),
          //                   decoration: BoxDecoration(
          //                     color: DARK_BLUE_COLOR,
          //                     borderRadius: BorderRadius.circular(10),
          //                   ),
          //                   child: Row(
          //                     children: [
          //                       Text('photo'),
          //                       Column(
          //                         crossAxisAlignment: CrossAxisAlignment.start,
          //                         children: [
          //                           Text(
          //                             '${guideCard?.name}',
          //                             style: TextStyle(color: Colors.white).merge(
          //                                 CustomTextStyle.normalTitleFontStyle),
          //                           ),
          //                           Text(
          //                             '${guideCard?.degree}',
          //                             style: TextStyle(color: LIGHT_BLUE_COLOR)
          //                                 .merge(CustomTextStyle
          //                                     .feedbackCountFontStyle),
          //                           ),
          //                           Row(
          //                             mainAxisAlignment:
          //                                 MainAxisAlignment.spaceBetween,
          //                             children: [
          //                               Row(
          //                                 children: [
          //                                   SvgPicture.asset(
          //                                       'assets/icons/star.svg',
          //                                       color: GOLDEN_COLOR),
          //                                   const SizedBox(
          //                                     width: 5,
          //                                   ),
          //                                   Text(
          //                                     '${guideCard?.rating}',
          //                                     style: TextStyle().merge(
          //                                         CustomTextStyle
          //                                             .ratingFontStyle),
          //                                   ),
          //                                 ],
          //                               ),
          //                               TextButton(
          //                                 onPressed: () {},
          //                                 child: Text(
          //                                   'Подробнее',
          //                                   style: TextStyle().merge(
          //                                       CustomTextStyle.btnFontStyle),
          //                                 ),
          //                               )
          //                             ],
          //                           ),
          //                         ],
          //                       )
          //                     ],
          //                   ),
          //                 );
          //               });
          //         },
          //       ),
          //     ],
          //   ),
          // ),
          ),
    );
  }
}
