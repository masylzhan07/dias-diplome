import 'package:flutter/material.dart';

class FAQScreen extends StatelessWidget {
  const FAQScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Map<String, dynamic>> _list = [
      {
        'question': 'Я могу выбрать сразу несколько товаров для покупки?',
        'answer':
            'Да. Вы можете указать количество при заказе товаров в корзине',
      },
      {
        'question':
            'Мне не привезли вовремя товар, купленный в Магазине на Kaspi.kz. Почему?',
        'answer': 'Напишите или позвоните нам по указанному номеру.',
      },
      {
        'question': 'У меня не получается оформить заказ. Что делать?',
        'answer':
            'Напишите или позвоните нам по указанному номеру, мы постараемся вам помочь.',
      },
      {
        'question': 'Хочу вернуть товар. Куда мне обратиться?',
        'answer':
            'Для возврата товара вам необходимо обратиться по указанному номеру.\n\nВ соответствии с законодательством, вы можете вернуть товар в течение 14 дней после покупки.'
      },
      {
        'question':
            'Что мне делать, если товар в магазине партнёра не соответствует описанию выбранного мной товара?',
        'answer':
            'Если вам не понравился товар или он не соответствует вашим ожиданиям, вы можете отказаться от покупки, сообщив причину менеджеру магазина.',
      },
    ];
    return Scaffold(
      extendBody: true,
      body: ListView.builder(
        padding:
            const EdgeInsets.only(bottom: 130, top: 30, left: 30, right: 30),
        itemCount: _list.length,
        itemBuilder: (context, index) {
          return ExpansionTile(
            title: Text(
              '${_list[index]['question']}',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w700,
              ),
            ),
            tilePadding: const EdgeInsets.symmetric(vertical: 10),
            expandedCrossAxisAlignment: CrossAxisAlignment.start,
            expandedAlignment: Alignment.centerLeft,
            childrenPadding:
                EdgeInsets.only(left: 17, right: 17, bottom: 10, top: 10),
            children: [
              Text(
                '${_list[index]['answer']}',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
