import 'package:dias_diplome/data/models/store_model.dart';
import 'package:dias_diplome/data/models/user_model.dart';
import 'package:dias_diplome/data/repositories/profile_repository.dart';
import 'package:dias_diplome/data/repositories/store_repository.dart';
import 'package:dias_diplome/logic/cubit/session_logic/session_cubit.dart';
import 'package:dias_diplome/presentation/screens/faq_screen.dart';
import 'package:dias_diplome/presentation/screens/products_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StoreScreen extends StatefulWidget {
  final int storeId;
  const StoreScreen({Key? key, required this.storeId}) : super(key: key);

  @override
  _StoreScreenState createState() => _StoreScreenState();
}

class _StoreScreenState extends State<StoreScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: FutureBuilder(
        future: StoreRepository().fetchStore(widget.storeId),
        builder: (BuildContext context, AsyncSnapshot<StoreModel> snapshot) {
          if (snapshot.hasData) {
            return SingleChildScrollView(
              padding: EdgeInsets.only(bottom: 130),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  snapshot.data!.background_image!.isEmpty
                      ? Container(
                          height: MediaQuery.of(context).size.height * 0.5,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey,
                          alignment: Alignment.center,
                          child: Text('Нет фото'),
                        )
                      : Image.network(
                          '${snapshot.data!.background_image}',
                          height: MediaQuery.of(context).size.height * 0.5,
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.cover,
                        ),
                  SizedBox(height: 30),
                  ...[
                    {
                      'tag': 'Название: ',
                      'name': snapshot.data!.name,
                    },
                    // {
                    //   'tag': 'Номер: ',
                    //   'name': snapshot.data!.phone,
                    // },
                    {
                      'tag': 'Адрес: ',
                      'name': snapshot.data!.address,
                    },
                  ]
                      .map(
                        (val) => Container(
                          margin: EdgeInsets.only(bottom: 20),
                          child: Row(
                            children: [
                              Text(
                                '${val['tag']}',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Text(
                                '${val['name']}',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                      .toList(),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).push(
                          CupertinoPageRoute(builder: (_) => FAQScreen()));
                    },
                    child: Text(
                      'FAQ',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(CupertinoPageRoute(
                            builder: (_) => ProductsScreen(
                                  storeId: widget.storeId,
                                )));
                      },
                      child: Text('Показать товары магазина'),
                    ),
                  ),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Ошибка'),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
