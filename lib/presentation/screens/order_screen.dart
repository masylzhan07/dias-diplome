import 'package:dias_diplome/data/models/order_model.dart';
import 'package:dias_diplome/data/providers/api.dart';
import 'package:dias_diplome/data/repositories/order_repository.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class OrderScreen extends StatelessWidget {
  const OrderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: OrderRepository().fetchOrder(),
        builder: (context, AsyncSnapshot<OrderModel> snapshot) {
          if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 100),
              child: WebView(
                initialUrl:
                    'https://zara.profcleaning.kz/orders/${snapshot.data!.id}',
              ),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Ошибка'),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
