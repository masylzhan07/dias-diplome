import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/models/material_model.dart';
import 'package:dias_diplome/data/models/product_model.dart';
import 'package:dias_diplome/data/repositories/colors_repository.dart';
import 'package:dias_diplome/data/repositories/products_repository.dart';
import 'package:dias_diplome/presentation/screens/seller/product_create_screen.dart';
import 'package:dias_diplome/presentation/screens/seller/product_seller_screen.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductsScreen extends StatefulWidget {
  final int? categoryId;
  final int? storeId;

  const ProductsScreen({Key? key, this.storeId, this.categoryId})
      : super(key: key);

  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  late List<Product> _products;
  List<int?>? _colorsId;
  String priceFrom = '';
  String priceTo = '';
  String? priceRange;
  List<MaterialModel>? _selectedColors;

  bool _isHandleRequest = true;
  bool _isHandleError = false;

  _getColors(List<MaterialModel> items) {
    setState(() {
      _colorsId = items.map((item) => item.id).toList();
      _selectedColors = items;
    });
  }

  _getFromPrice(String fromPrice) {
    setState(() {
      priceFrom = fromPrice;
    });
  }

  _getToPrice(String toPrice) {
    setState(() {
      priceTo = toPrice;
    });
  }

  _acceptFilters() {
    scaffoldKey.currentState!.openDrawer();
    setState(() {
      _isHandleRequest = true;
      if (priceFrom.isNotEmpty && priceTo.isNotEmpty) {
        priceRange = '$priceFrom..$priceTo';
      } else if (priceFrom.isNotEmpty) {
        priceRange = '$priceFrom..99999999999';
      } else if (priceTo.isNotEmpty) {
        priceRange = '0..$priceTo';
      } else {
        priceRange = null;
      }
    });
  }

  _loadData() async {
    try {
      final responseProducts = await ProductsRepository().fetchProducts(
        storeId: widget.storeId,
        categoryId: widget.categoryId,
        colors: _colorsId,
        priceRange: priceRange,
      );
      setState(() {
        _products = responseProducts;
        _isHandleRequest = false;
        _isHandleError = false;
      });
    } catch (e) {
      setState(() {
        _products = [];
        _isHandleRequest = false;
        _isHandleError = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      extendBody: true,
      endDrawerEnableOpenDragGesture: false,
      endDrawer: FilterDrawer(
        getColors: _getColors,
        selectedItems: _selectedColors,
        acceptFilters: _acceptFilters,
        getFromPrice: _getFromPrice,
        getToPrice: _getToPrice,
        initialPriceFrom: priceFrom,
        initialPriceTo: priceTo,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Flexible(
                  child: Text(
                    'Список товаров',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                const SizedBox(width: 20),
                IconButton(
                  onPressed: () {
                    scaffoldKey.currentState!.openEndDrawer();
                  },
                  splashRadius: 24,
                  icon: const Icon(
                    Icons.filter_list_alt,
                    size: 30,
                    color: PRIMARY_COLOR,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: _bodyList(),
          ),
        ],
      ),
    );
  }

  Widget _bodyList() {
    if (_isHandleRequest) {
      _loadData();
      return Center(
        child: CircularProgressIndicator(),
      );
    } else if (_isHandleError) {
      return Center(
        child: Text('Ошибка'),
      );
    }
    if (_products.isEmpty) {
      return Center(
        child: Text('Список пуст'),
      );
    }
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        childAspectRatio: 121 / 210,
      ),
      itemCount: _products.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            Navigator.of(context).push(CupertinoPageRoute(
                builder: (_) => ProductSellerScreen(
                      product: _products[index],
                    )));
          },
          child: Container(
            margin: EdgeInsets.all(30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.network(
                  '${_products[index].images!.first.src}',
                  height: 110,
                  fit: BoxFit.cover,
                ),
                SizedBox(height: 10),
                Text('${_products[index].name}'),
                SizedBox(height: 5),
                Text('${_products[index].category!.name}'),
                SizedBox(height: 5),
                Text('${_products[index].price}'),
                SizedBox(height: 5),
                if (_products[index].colors!.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: Text('${_products[index].colors?.first.name}'),
                  ),
                Text('${_products[index].size!.name}'),
              ],
            ),
          ),
        );
      },
    );
  }
}

class FilterDrawer extends StatelessWidget {
  final Function getColors;
  final Function getFromPrice;
  final Function getToPrice;
  final Function acceptFilters;
  final List<MaterialModel>? selectedItems;
  final String? initialPriceFrom;
  final String? initialPriceTo;
  const FilterDrawer({
    Key? key,
    required this.getColors,
    required this.getFromPrice,
    required this.getToPrice,
    required this.acceptFilters,
    this.selectedItems,
    this.initialPriceFrom,
    this.initialPriceTo,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: PRIMARY_COLOR,
      child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(bottom: 130.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Фильтр',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 30),
              FutureBuilder(
                future: ColorsRepository().fetchColors(),
                builder:
                    (context, AsyncSnapshot<List<MaterialModel>> snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Цена',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(height: 10),
                        Row(
                          children: [
                            Flexible(
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 4),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.03),
                                      blurRadius: 4,
                                      offset: Offset(0, 4),
                                    ),
                                  ],
                                ),
                                child: TextFormField(
                                  initialValue: initialPriceFrom,
                                  decoration: const InputDecoration(
                                    hintText: 'От',
                                    border: InputBorder.none,
                                  ),
                                  onChanged: (value) => getFromPrice(value),
                                ),
                              ),
                            ),
                            const SizedBox(width: 20),
                            Flexible(
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 4),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.03),
                                      blurRadius: 4,
                                      offset: Offset(0, 4),
                                    ),
                                  ],
                                ),
                                child: TextFormField(
                                  initialValue: initialPriceTo,
                                  decoration: const InputDecoration(
                                    hintText: 'До',
                                    border: InputBorder.none,
                                  ),
                                  onChanged: (value) => getToPrice(value),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20),
                        const Text(
                          'Цвета',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(height: 10),
                        DropdownSearch<MaterialModel>.multiSelection(
                          items: snapshot.data,
                          selectedItems: selectedItems ?? snapshot.data!,
                          dropdownSearchDecoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: BorderSide.none,
                            ),
                            fillColor: Colors.white,
                            filled: true,
                          ),
                          // mode: Mode.MENU,
                          itemAsString: (item) {
                            return '${item!.name}';
                          },
                          onChanged: (items) => getColors(items),
                          // selectedItems: ["Brazil"],
                        ),
                        const SizedBox(height: 20),
                        OutlinedButton(
                          onPressed: () => acceptFilters(),
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.white),
                          ),
                          child: const Text(
                            'Поиск',
                            style: TextStyle(
                              color: PRIMARY_COLOR,
                            ),
                          ),
                        ),
                      ],
                    );
                  } else if (snapshot.hasError) {
                    return const Center(
                      child: Text('Ошибка'),
                    );
                  }
                  return const Center(
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
