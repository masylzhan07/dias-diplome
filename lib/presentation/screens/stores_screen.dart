import 'package:dias_diplome/data/models/store_model.dart';
import 'package:dias_diplome/data/repositories/stores_repositories.dart';
import 'package:dias_diplome/presentation/screens/store_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StoresScreen extends StatelessWidget {
  const StoresScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: FutureBuilder(
          future: StoresRepository().fetchStores(),
          builder:
              (BuildContext context, AsyncSnapshot<List<StoreModel>> snapshot) {
            if (snapshot.hasData) {
              print(snapshot.data);
              return ListView.builder(
                  padding: EdgeInsets.only(top: 20, bottom: 130),
                  itemCount: snapshot.data?.length ?? 0,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(CupertinoPageRoute(
                            builder: (_) => StoreScreen(
                                storeId: snapshot.data![index].id!)));
                      },
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 30),
                        child: Stack(
                          children: [
                            SizedBox(
                              height: 200,
                              width: MediaQuery.of(context).size.width,
                              child: snapshot
                                      .data![index].background_image!.isEmpty
                                  ? Image.asset(
                                      'assets/images/placeholder.png',
                                      fit: BoxFit.cover,
                                    )
                                  : Image.network(
                                      '${snapshot.data![index].background_image}',
                                      fit: BoxFit.cover,
                                    ),
                            ),
                            Positioned.fill(
                              child: Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Color.fromRGBO(0, 0, 0, 0.1),
                                      Color.fromRGBO(0, 0, 0, 0.5),
                                    ],
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 20,
                              left: 20,
                              right: 20,
                              child: Text(
                                '${snapshot.data![index].name}',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  });
            } else if (snapshot.hasError) {
              print(snapshot.error);
              return Center(
                child: Text('Error'),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }
}
