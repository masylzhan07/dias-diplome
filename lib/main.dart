import 'package:dias_diplome/constants/colors.dart';
import 'package:dias_diplome/data/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'data/repositories/auth_repository.dart';
import 'logic/cubit/session_logic/session_cubit.dart';
import 'presentation/screens/authorization_screen.dart';
import 'presentation/screens/home_screen.dart';
import 'presentation/widgets/app_navigator.dart';
import 'presentation/widgets/bottom_navbar.dart';

void main() async {
  //Метод используется для выполнения кода до запуска приложения
  WidgetsFlutterBinding.ensureInitialized();

//чтение с диска
  final prefs = await SharedPreferences.getInstance();

//проверка на первый запуск, если первый запуск, то удаляет все данные (а именно токен пользователя, если был)
  if (prefs.getBool('first_run') ?? true) {
    FlutterSecureStorage storage = FlutterSecureStorage();

    await storage.deleteAll();

    prefs.setBool('first_run', false);
  }

  //запуск приложения
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //делаем ориентацию приложения только портретным, отключаем возможность поворот экрана
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    // Настройка интерфейса приложения
    return MaterialApp(
      title: 'Online Shop',
      debugShowCheckedModeBanner: false,
      //оснвоной цвет и шрифт
      theme: ThemeData(
        primaryColor: PRIMARY_COLOR,
        fontFamily: 'MontSerrat',
      ),
      home: MultiRepositoryProvider(
        providers: [
          //репозитории аутентификации,
          RepositoryProvider(create: (context) => AuthRepository()),
        ],
        //provider используется для доступа к чтению, обнволению и т.д. данных с любого виджета
        child: MultiProvider(
          providers: [
            BlocProvider(
              create: (context) => SessionCubit(
                authRepo: context.read<AuthRepository>(),
              ),
            ),
            ChangeNotifierProvider(create: (context) => UserProvider()),
          ],
          //внутри виджета происходит проверка на аутентификацию пользователя
          child: AppNavigator(),
        ),
      ),
      // Таблица маршрутизации верхнего уровня приложения.
      routes: {
        AuthorizationScreen.routeName: (context) => AuthorizationScreen(),
        CustomBottomNavigation.routeName: (context) =>
            const CustomBottomNavigation(),
        HomeScreen.routeName: (context) => const HomeScreen(),
      },
    );
  }
}
