import 'package:flutter/material.dart';

class CustomTextStyle {
  static TextStyle get descriptionFontStyle {
    return TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w400,
      height: 1.56,
    );
  }
}
