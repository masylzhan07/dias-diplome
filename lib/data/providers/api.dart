import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class APIRepository {
  late Dio _client;
  final FlutterSecureStorage _storage = const FlutterSecureStorage();

  APIRepository() {
    _client = Dio()
      ..options.baseUrl = 'https://zara.profcleaning.kz/api'
      ..interceptors.add(
        InterceptorsWrapper(onRequest: (RequestOptions option,
            RequestInterceptorHandler interceptorHandler) async {
          // _storage.deleteAll();
          String? token = await _storage.read(key: "accessToken");
          option.headers.addAll({
            "Authorization": "Bearer $token",
            // "Accept": "application/json",
          });

          return interceptorHandler.next(option);
        }, onError: (DioError error, ErrorInterceptorHandler handler) {
          print(error);
        }),
      );
  }
  Future<Response> logIn({String? username, String? password}) async {
    return await Dio().post('https://zara.profcleaning.kz/api/v1/login',
        data: {"username": username, "password": password});
  }

  Future<Response> signUp({
    String? email,
    String? password,
    int? roleId,
    String? name,
  }) async {
    return await Dio()
        .post('https://zara.profcleaning.kz/api/v1/registration', data: {
      "email": email,
      "password": password,
      "role_id": roleId,
      if (roleId == 102) 'name_of_shop': name,
      if (roleId == 103) 'name': name,
    });
  }

  Future<Response> fetchProfile() async {
    return await _client.get('/v1/user');
  }

  Future<Response> updateProfile(
      int? userId, Map<String, dynamic> params) async {
    return await _client.put('/v1/users/$userId', queryParameters: params);
  }

  Future<Response> updateSellerProfile(int? userId, FormData formData) async {
    print(userId);
    print(formData.fields);
    return await _client.post('/v1/users/$userId', data: formData);
  }

  Future<Response> fetchProducts(Map<String, dynamic> params) async {
    return await _client.get(
      '/v1/products?per_page=99999',
      queryParameters: params,
    );
  }

  Future<Response> createProduct(FormData formData) async {
    try {
      return await _client.post('/v1/products', data: formData);
    } catch (e) {
      print(e);
      return throw Exception(e);
    }
  }

  Future<Response> fetchCategories({int? parentId}) async {
    return await _client.get(
      '/v1/categories?per_page=99999',
      queryParameters: {'filter[parent_id]': parentId},
    );
  }

  Future<Response> fetchMaterials() async {
    return await _client.get('/v1/materials?per_page=99999');
  }

  Future<Response> fetchColors() async {
    return await _client.get('/v1/colors?per_page=99999');
  }

  Future<Response> fetchSizes() async {
    return await _client.get('/v1/sizes?per_page=99999');
  }

  Future<Response> fetchStores() async {
    return await _client.get('/v1/stores?per_page=99999');
  }

  Future<Response> fetchStore(int storeId) async {
    return await _client.get('/v1/stores/$storeId');
  }

  Future<Response> addToBasket(FormData formData) async {
    return await _client.post('/v1/baskets', data: formData);
  }

  Future<Response> getBasket(Map<String, dynamic> params) async {
    return await _client.get('/v1/baskets', queryParameters: params);
  }

  Future<Response> changeProductCountInBasket(
      int basketId, Map<String, dynamic> formData) async {
    return await _client.put('/v1/baskets/$basketId',
        queryParameters: formData);
  }

  Future<Response> getOrders() async {
    return await _client.post('/v1/orders');
  }

  Future<Response> payForm(FormData data) async {
    return await _client.post('https://epay.kkb.kz/jsp/client/pay.jsp',
        data: data);
  }

  Future<Response> getAnalytics() async {
    return await _client.get('/v1/analytics');
  }
}
