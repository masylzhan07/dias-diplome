import 'package:flutter/material.dart';

class UserProvider with ChangeNotifier {
  int? _roleId;
  int? _userId;
  String? _deviceId;

  int? get roleId => _roleId;
  int? get userId => _userId;
  String? get deviceId => _deviceId;

  void setRoleId(int id) {
    _roleId = id;
    notifyListeners();
  }

  void setUserId(int id) {
    _userId = id;
    notifyListeners();
  }

  void setDeviceId(String id) {
    _deviceId = id;
    // notifyListeners();
  }
}
