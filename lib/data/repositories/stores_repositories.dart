import 'dart:async';

import 'package:dias_diplome/data/models/category_model.dart';
import 'package:dias_diplome/data/models/product_model.dart';
import 'package:dias_diplome/data/models/store_model.dart';
import 'package:dias_diplome/data/providers/api.dart';

class StoresRepository {
  Future<List<StoreModel>> fetchStores() async {
    try {
      final result = await APIRepository().fetchStores();

      if (result.statusCode == 200) {
        // return Product.fromMap(result.data['data']);
        return (result.data['data'] as List)
            .map((store) => StoreModel.fromMap(store))
            .toList();
      } else {
        throw Exception('Error on fetchStores: ${result.statusCode}');
      }
    } on Exception catch (e) {
      throw Exception('Error: $e');
    }
  }
}
