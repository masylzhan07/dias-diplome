import 'dart:async';

import 'package:dias_diplome/data/models/analytics_model.dart';
import 'package:dias_diplome/data/providers/api.dart';

class AnalyticsRepository {
  Future<AnalyticsModel> fetchAnalytics() async {
    try {
      final result = await APIRepository().getAnalytics();

      if (result.statusCode == 200) {
        return AnalyticsModel.fromMap(result.data['data']);
      } else {
        throw Exception('Error on fetchProducts: ${result.statusCode}');
      }
    } on Exception catch (e) {
      throw Exception('Error: $e');
    }
  }
}
