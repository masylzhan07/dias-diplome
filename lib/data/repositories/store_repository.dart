import 'dart:async';

import 'package:dias_diplome/data/models/store_model.dart';
import 'package:dias_diplome/data/providers/api.dart';

class StoreRepository {
  Future<StoreModel> fetchStore(int storeId) async {
    try {
      final result = await APIRepository().fetchStore(storeId);

      if (result.statusCode == 200) {
        return StoreModel.fromMap(result.data['data']);
      } else {
        throw Exception('Error on fetchStore: ${result.statusCode}');
      }
    } on Exception catch (e) {
      throw Exception('Error: $e');
    }
  }
}
