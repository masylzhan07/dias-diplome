import 'dart:async';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:dias_diplome/data/models/basket_model.dart';
import 'package:dias_diplome/data/providers/api.dart';

class BasketRepository {
  Future<BasketModel> fetchBasketProducts(int? userId, String? deviceId) async {
    // const FlutterSecureStorage _storage = FlutterSecureStorage();
    // String? id = await _storage.read(key: 'accessToken');

    // throw Exception('Error: ');

    Map<String, dynamic> params = {};
    params['deviceId'] = deviceId;
    if (userId != null) params['userId'] = userId;
    try {
      final result = await APIRepository().getBasket(params);
      if (result.statusCode == 200) {
        if (result.data['data'].isNotEmpty) {
          return BasketModel.fromMap(result.data['data'][0]);
        } else {
          return BasketModel(id: 0, app_number: '-', products: []);
        }
      } else {
        throw Exception('Error on fetchBasket: ${result.statusCode}');
      }
    } on Exception catch (e) {
      throw Exception('Error: $e');
    }
  }
}
