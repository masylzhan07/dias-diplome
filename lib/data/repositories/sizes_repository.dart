import 'dart:async';

import 'package:dias_diplome/data/models/material_model.dart';
import 'package:dias_diplome/data/providers/api.dart';

class SizesRepository {
  Future<List<MaterialModel>> fetchSizes() async {
    try {
      final result = await APIRepository().fetchSizes();

      if (result.statusCode == 200) {
        // return Product.fromMap(result.data['data']);
        return (result.data['data'] as List)
            .map((material) => MaterialModel.fromMap(material))
            .toList();
      } else {
        throw Exception('Error on fetchSizes: ${result.statusCode}');
      }
    } on Exception catch (e) {
      throw Exception('Error: $e');
    }
  }
}
