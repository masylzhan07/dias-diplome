import 'dart:async';
import 'package:dias_diplome/data/models/order_model.dart';
import 'package:dias_diplome/data/providers/api.dart';

class OrderRepository {
  Future<OrderModel> fetchOrder() async {
    try {
      final result = await APIRepository().getOrders();
      if (result.statusCode == 200) {
        return OrderModel.fromMap(result.data['data']);
      } else {
        throw Exception('Error on fetchOrder: ${result.statusCode}');
      }
    } on Exception catch (e) {
      throw Exception('Error: $e');
    }
  }
}
