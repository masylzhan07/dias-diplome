import 'dart:async';

import 'package:dias_diplome/data/models/product_model.dart';
import 'package:dias_diplome/data/providers/api.dart';

class ProductsRepository {
  Future<List<Product>> fetchProducts({
    int? storeId,
    int? categoryId,
    List<int?>? colors,
    String? priceRange,
  }) async {
    Map<String, dynamic> params = {};
    if (storeId != null) params['filter[store_id]'] = storeId;
    if (categoryId != null) params['filter[category_id]'] = categoryId;
    if (colors != null) params['filter[colors]'] = colors.join(',');
    if (priceRange != null) params['filter[price]'] = priceRange;

    print(params);
    try {
      final result = await APIRepository().fetchProducts(params);

      if (result.statusCode == 200) {
        // return Product.fromMap(result.data['data']);
        print(result.data['data']);
        return (result.data['data'] as List)
            .map((product) => Product.fromMap(product))
            .toList();
      } else {
        throw Exception('Error on fetchProducts: ${result.statusCode}');
      }
    } on Exception catch (e) {
      throw Exception('Error: $e');
    }
  }
}
