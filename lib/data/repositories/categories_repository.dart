import 'dart:async';

import 'package:dias_diplome/data/models/category_model.dart';
import 'package:dias_diplome/data/models/product_model.dart';
import 'package:dias_diplome/data/providers/api.dart';

class CategoriesRepository {
  Future<List<Category>> fetchCategories({int? parentId}) async {
    try {
      final result = await APIRepository().fetchCategories(parentId: parentId);

      if (result.statusCode == 200) {
        // return Product.fromMap(result.data['data']);
        return (result.data['data'] as List)
            .map((category) => Category.fromMap(category))
            .toList();
      } else {
        throw Exception('Error on fetchCategories: ${result.statusCode}');
      }
    } on Exception catch (e) {
      throw Exception('Error: $e');
    }
  }
}
