import 'dart:async';

import 'package:dias_diplome/data/models/user_model.dart';
import 'package:dias_diplome/data/providers/api.dart';

class ProfileRepository {
  Future<User> fetchProfile() async {
    try {
      final result = await APIRepository().fetchProfile();

      if (result.statusCode == 200) {
        return User.fromMap(result.data['data']);
      } else {
        throw Exception('Error on fetchProfile: ${result.statusCode}');
      }
    } on Exception catch (e) {
      throw Exception('Error: $e');
    }
  }
}
