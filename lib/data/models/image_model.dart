import 'dart:convert';

import 'package:flutter/foundation.dart';

class ImageModel {
  List<String>? name;
  String? src;
  String? hash;
  ImageModel({
    this.name,
    this.src,
    this.hash,
  });

  ImageModel copyWith({
    List<String>? name,
    String? src,
    String? hash,
  }) {
    return ImageModel(
      name: name ?? this.name,
      src: src ?? this.src,
      hash: hash ?? this.hash,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'src': src,
      'hash': hash,
    };
  }

  factory ImageModel.fromMap(Map<String, dynamic> map) {
    return ImageModel(
      name: List<String>.from(map['name']),
      src: "https://zara.profcleaning.kz/${map['src']}",
      hash: map['hash'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ImageModel.fromJson(String source) =>
      ImageModel.fromMap(json.decode(source));

  @override
  String toString() => 'ImageModel(name: $name, src: $src, hash: $hash)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ImageModel &&
        listEquals(other.name, name) &&
        other.src == src &&
        other.hash == hash;
  }

  @override
  int get hashCode => name.hashCode ^ src.hashCode ^ hash.hashCode;
}
