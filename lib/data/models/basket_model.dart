import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:dias_diplome/data/models/product_model.dart';

class BasketModel {
  final int? id;
  final String? app_number;
  final List<Product> products;
  BasketModel({
    this.id,
    this.app_number,
    required this.products,
  });

  BasketModel copyWith({
    int? id,
    String? app_number,
    List<Product>? products,
  }) {
    return BasketModel(
      id: id ?? this.id,
      app_number: app_number ?? this.app_number,
      products: products ?? this.products,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'app_number': app_number,
      'products': products.map((x) => x.toMap()).toList(),
    };
  }

  factory BasketModel.fromMap(Map<String, dynamic> map) {
    return BasketModel(
      id: map['id']?.toInt(),
      app_number: map['app_number'],
      products:
          List<Product>.from(map['products']?.map((x) => Product.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory BasketModel.fromJson(String source) =>
      BasketModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'BasketModel(id: $id, app_number: $app_number, products: $products)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is BasketModel &&
        other.id == id &&
        other.app_number == app_number &&
        listEquals(other.products, products);
  }

  @override
  int get hashCode => id.hashCode ^ app_number.hashCode ^ products.hashCode;
}
