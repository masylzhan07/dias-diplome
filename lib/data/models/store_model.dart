import 'dart:convert';

class StoreModel {
  final int? id;
  final String? name;
  final String? background_image;
  final String? address;
  StoreModel({
    this.id,
    this.name,
    this.background_image,
    this.address,
  });

  StoreModel copyWith({
    int? id,
    String? name,
    String? background_image,
    String? address,
  }) {
    return StoreModel(
      id: id ?? this.id,
      name: name ?? this.name,
      background_image: background_image ?? this.background_image,
      address: address ?? this.address,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'background_image': background_image,
      'address': address,
    };
  }

  factory StoreModel.fromMap(Map<String, dynamic> map) {
    return StoreModel(
      id: map['id']?.toInt() ?? -1,
      name: map['name'] ?? '-',
      background_image:
          map['background_image'] != null ? map['background_image'] : '',
      address: map['address'] ?? '-',
    );
  }

  String toJson() => json.encode(toMap());

  factory StoreModel.fromJson(String source) =>
      StoreModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'StoreModel(id: $id, name: $name, background_image: $background_image, address: $address)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is StoreModel &&
        other.id == id &&
        other.name == name &&
        other.background_image == background_image &&
        other.address == address;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        background_image.hashCode ^
        address.hashCode;
  }
}
