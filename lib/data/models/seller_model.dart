import 'dart:convert';

import 'role_model.dart';

class SellerModel {
  int? id;
  String? name_of_shop;
  String? name;
  String? email;
  String? phone;
  String? background_image_of_shop;
  String? address_of_shop;
  String? email_verified;
  Role? role;
  SellerModel({
    this.id,
    this.name_of_shop,
    this.name,
    this.email,
    this.phone,
    this.background_image_of_shop,
    this.address_of_shop,
    this.email_verified,
    this.role,
  });

  SellerModel copyWith({
    int? id,
    String? name_of_shop,
    String? name,
    String? email,
    String? phone,
    String? background_image_of_shop,
    String? address_of_shop,
    String? email_verified,
    Role? role,
  }) {
    return SellerModel(
      id: id ?? this.id,
      name_of_shop: name_of_shop ?? this.name_of_shop,
      name: name ?? this.name,
      email: email ?? this.email,
      phone: phone ?? this.phone,
      background_image_of_shop:
          background_image_of_shop ?? this.background_image_of_shop,
      address_of_shop: address_of_shop ?? this.address_of_shop,
      email_verified: email_verified ?? this.email_verified,
      role: role ?? this.role,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name_of_shop': name_of_shop,
      'name': name,
      'email': email,
      'phone': phone,
      'background_image_of_shop': background_image_of_shop,
      'address_of_shop': address_of_shop,
      'email_verified': email_verified,
      'role': role?.toMap(),
    };
  }

  factory SellerModel.fromMap(Map<String, dynamic> map) {
    return SellerModel(
      id: map['id']?.toInt() ?? 0,
      name_of_shop: map['name_of_shop'] ?? '',
      name: map['name'] ?? '',
      email: map['email'] ?? '',
      phone: map['phone'] ?? '',
      background_image_of_shop: map['background_image_of_shop'] ?? '',
      address_of_shop: map['address_of_shop'] ?? '',
      email_verified: map['email_verified'] ?? '',
      role: map['role'] != null ? Role.fromMap(map['role']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory SellerModel.fromJson(String source) =>
      SellerModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'SellerModel(id: $id, name_of_shop: $name_of_shop, name: $name, email: $email, phone: $phone, background_image_of_shop: $background_image_of_shop, address_of_shop: $address_of_shop, email_verified: $email_verified, role: $role)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SellerModel &&
        other.id == id &&
        other.name_of_shop == name_of_shop &&
        other.name == name &&
        other.email == email &&
        other.phone == phone &&
        other.background_image_of_shop == background_image_of_shop &&
        other.address_of_shop == address_of_shop &&
        other.email_verified == email_verified &&
        other.role == role;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name_of_shop.hashCode ^
        name.hashCode ^
        email.hashCode ^
        phone.hashCode ^
        background_image_of_shop.hashCode ^
        address_of_shop.hashCode ^
        email_verified.hashCode ^
        role.hashCode;
  }
}
