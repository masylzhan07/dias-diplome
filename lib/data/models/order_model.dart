import 'dart:convert';

class OrderModel {
  final int? id;
  OrderModel({
    this.id,
  });

  OrderModel copyWith({
    int? id,
  }) {
    return OrderModel(
      id: id ?? this.id,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
    };
  }

  factory OrderModel.fromMap(Map<String, dynamic> map) {
    return OrderModel(
      id: map['id']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory OrderModel.fromJson(String source) =>
      OrderModel.fromMap(json.decode(source));

  @override
  String toString() => 'OrderModel(id: $id)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is OrderModel && other.id == id;
  }

  @override
  int get hashCode => id.hashCode;
}
