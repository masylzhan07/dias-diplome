import 'dart:convert';

import 'package:flutter/foundation.dart';

class Category {
  int? id;
  String? name;
  final CategoryParent? parent;
  List<Category>? childs;
  String? image;
  Category({
    this.id,
    this.name,
    this.parent,
    required this.childs,
    this.image,
  });

  Category copyWith({
    int? id,
    String? name,
    CategoryParent? parent,
    List<Category>? childs,
    String? image,
  }) {
    return Category(
      id: id ?? this.id,
      name: name ?? this.name,
      parent: parent ?? this.parent,
      childs: childs ?? this.childs,
      image: image ?? this.image,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'parent': parent?.toMap(),
      'childs': childs?.map((x) => x.toMap()).toList(),
      'image': image,
    };
  }

  factory Category.fromMap(Map<String, dynamic> map) {
    return Category(
      id: map['id']?.toInt(),
      name: map['name'],
      parent:
          map['parent'] != null ? CategoryParent.fromMap(map['parent']) : null,
      childs: map['childs'] != null
          ? List<Category>.from(map['childs']?.map((x) => Category.fromMap(x)))
          : null,
      image: map['image'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Category.fromJson(String source) =>
      Category.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Category(id: $id, name: $name, parent: $parent, childs: $childs, image: $image)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Category &&
        other.id == id &&
        other.name == name &&
        other.parent == parent &&
        listEquals(other.childs, childs) &&
        other.image == image;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        parent.hashCode ^
        childs.hashCode ^
        image.hashCode;
  }
}

class CategoryParent {
  final int? id;
  final String? name;
  CategoryParent({
    this.id,
    this.name,
  });

  CategoryParent copyWith({
    int? id,
    String? name,
  }) {
    return CategoryParent(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory CategoryParent.fromMap(Map<String, dynamic> map) {
    return CategoryParent(
      id: map['id']?.toInt(),
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoryParent.fromJson(String source) =>
      CategoryParent.fromMap(json.decode(source));

  @override
  String toString() => 'CategoryParent(id: $id, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CategoryParent && other.id == id && other.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
