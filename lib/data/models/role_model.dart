import 'dart:convert';

class Role {
  int? id;
  String? slug;
  String? name;
  Role({
    this.id,
    this.slug,
    this.name,
  });

  Role copyWith({
    int? id,
    String? slug,
    String? name,
  }) {
    return Role(
      id: id ?? this.id,
      slug: slug ?? this.slug,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'slug': slug,
      'name': name,
    };
  }

  factory Role.fromMap(Map<String, dynamic> map) {
    return Role(
      id: map['id']?.toInt(),
      slug: map['slug'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Role.fromJson(String source) => Role.fromMap(json.decode(source));

  @override
  String toString() => 'Role(id: $id, slug: $slug, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Role &&
        other.id == id &&
        other.slug == slug &&
        other.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ slug.hashCode ^ name.hashCode;
}
