import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:dias_diplome/data/models/product_model.dart';

class AnalyticsModel {
  final int? total;
  final int? profit;
  final List<Product>? products;
  AnalyticsModel({
    this.total,
    this.profit,
    this.products,
  });

  AnalyticsModel copyWith({
    int? total,
    int? profit,
    List<Product>? products,
  }) {
    return AnalyticsModel(
      total: total ?? this.total,
      profit: profit ?? this.profit,
      products: products ?? this.products,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'total': total,
      'profit': profit,
      'products': products?.map((x) => x.toMap()).toList(),
    };
  }

  factory AnalyticsModel.fromMap(Map<String, dynamic> map) {
    return AnalyticsModel(
      total: map['total']?.toInt(),
      profit: map['profit']?.toInt(),
      products: map['products'] != null
          ? List<Product>.from(map['products']?.map((x) => Product.fromMap(x)))
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory AnalyticsModel.fromJson(String source) =>
      AnalyticsModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'AnalyticsModel(total: $total, profit: $profit, products: $products)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AnalyticsModel &&
        other.total == total &&
        other.profit == profit &&
        listEquals(other.products, products);
  }

  @override
  int get hashCode => total.hashCode ^ profit.hashCode ^ products.hashCode;
}
