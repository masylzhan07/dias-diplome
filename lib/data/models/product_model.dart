import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:dias_diplome/data/models/image_model.dart';
import 'package:dias_diplome/data/models/material_model.dart';

class Product {
  int? id;
  String? name;
  MaterialModel? category;
  MaterialModel? brand;
  MaterialModel? store;
  MaterialModel? size;
  List<MaterialModel>? colors;
  List<MaterialModel>? materials;
  List<ImageModel>? images;
  String? price;
  int? count;
  PivotModel? pivot;
  int? sold;
  double? total;
  Product({
    this.id,
    this.name,
    this.category,
    this.brand,
    this.store,
    this.size,
    this.colors,
    this.materials,
    this.images,
    this.price,
    this.count,
    this.pivot,
    this.sold,
    this.total,
  });

  Product copyWith({
    int? id,
    String? name,
    MaterialModel? category,
    MaterialModel? brand,
    MaterialModel? store,
    MaterialModel? size,
    List<MaterialModel>? colors,
    List<MaterialModel>? materials,
    List<ImageModel>? images,
    String? price,
    int? count,
    PivotModel? pivot,
    int? sold,
    double? total,
  }) {
    return Product(
      id: id ?? this.id,
      name: name ?? this.name,
      category: category ?? this.category,
      brand: brand ?? this.brand,
      store: store ?? this.store,
      size: size ?? this.size,
      colors: colors ?? this.colors,
      materials: materials ?? this.materials,
      images: images ?? this.images,
      price: price ?? this.price,
      count: count ?? this.count,
      pivot: pivot ?? this.pivot,
      sold: sold ?? this.sold,
      total: total ?? this.total,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'category': category?.toMap(),
      'brand': brand?.toMap(),
      'store': store?.toMap(),
      'size': size?.toMap(),
      'colors': colors?.map((x) => x.toMap()).toList(),
      'materials': materials?.map((x) => x.toMap()).toList(),
      'images': images?.map((x) => x.toMap()).toList(),
      'price': price,
      'count': count,
      'pivot': pivot?.toMap(),
      'sold': sold,
      'total': total,
    };
  }

  factory Product.fromMap(Map<String, dynamic> map) {
    return Product(
      id: map['id']?.toInt(),
      name: map['name'],
      category: map['category'] != null
          ? MaterialModel.fromMap(map['category'])
          : null,
      brand: map['brand'] != null ? MaterialModel.fromMap(map['brand']) : null,
      store: map['store'] != null ? MaterialModel.fromMap(map['store']) : null,
      size: map['size'] != null ? MaterialModel.fromMap(map['size']) : null,
      colors: map['colors'] != null
          ? List<MaterialModel>.from(
              map['colors']?.map((x) => MaterialModel.fromMap(x)))
          : null,
      materials: map['materials'] != null
          ? List<MaterialModel>.from(
              map['materials']?.map((x) => MaterialModel.fromMap(x)))
          : null,
      images: map['images'] != null
          ? List<ImageModel>.from(
              map['images']?.map((x) => ImageModel.fromMap(x)))
          : null,
      price: map['price'],
      count: map['count']?.toInt(),
      pivot: map['pivot'] != null ? PivotModel.fromMap(map['pivot']) : null,
      sold: map['sold']?.toInt(),
      total: map['total']?.toDouble(),
    );
  }

  String toJson() => json.encode(toMap());

  factory Product.fromJson(String source) =>
      Product.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Product(id: $id, name: $name, category: $category, brand: $brand, store: $store, size: $size, colors: $colors, materials: $materials, images: $images, price: $price, count: $count, pivot: $pivot, sold: $sold, total: $total)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Product &&
        other.id == id &&
        other.name == name &&
        other.category == category &&
        other.brand == brand &&
        other.store == store &&
        other.size == size &&
        listEquals(other.colors, colors) &&
        listEquals(other.materials, materials) &&
        listEquals(other.images, images) &&
        other.price == price &&
        other.count == count &&
        other.pivot == pivot &&
        other.sold == sold &&
        other.total == total;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        category.hashCode ^
        brand.hashCode ^
        store.hashCode ^
        size.hashCode ^
        colors.hashCode ^
        materials.hashCode ^
        images.hashCode ^
        price.hashCode ^
        count.hashCode ^
        pivot.hashCode ^
        sold.hashCode ^
        total.hashCode;
  }
}

class PivotModel {
  final int? basket_id;
  final int? product_id;
  final int? count;
  PivotModel({
    this.basket_id,
    this.product_id,
    this.count,
  });

  PivotModel copyWith({
    int? basket_id,
    int? product_id,
    int? count,
  }) {
    return PivotModel(
      basket_id: basket_id ?? this.basket_id,
      product_id: product_id ?? this.product_id,
      count: count ?? this.count,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'basket_id': basket_id,
      'product_id': product_id,
      'count': count,
    };
  }

  factory PivotModel.fromMap(Map<String, dynamic> map) {
    return PivotModel(
      basket_id: map['basket_id']?.toInt(),
      product_id: map['product_id']?.toInt(),
      count: map['count']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory PivotModel.fromJson(String source) =>
      PivotModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'PivotModel(basket_id: $basket_id, product_id: $product_id, count: $count)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is PivotModel &&
        other.basket_id == basket_id &&
        other.product_id == product_id &&
        other.count == count;
  }

  @override
  int get hashCode => basket_id.hashCode ^ product_id.hashCode ^ count.hashCode;
}
